package com.dto.integration.dto;

import java.util.Date;

public class ArticuloDTO {
	
	private String clave;
	private double costoCompra;
	private double tipoCambio;
	private Date fecha;
	
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public double getCostoCompra() {
		return costoCompra;
	}
	public void setCostoCompra(double costoCompra) {
		this.costoCompra = costoCompra;
	}
	public double getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	

}
