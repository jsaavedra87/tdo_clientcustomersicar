package com.dto.integration.dto;

import java.util.List;

import com.tdo.integration.client.model.Cliente;
import com.tdo.integration.client.model.Credito;

public class CreditDTO {
	
	private static final long serialVersionUID = 1L;
	private String sucursal;
	private Cliente cliente;
	private List<Credito> creditos;

	public CreditDTO () {
		this.cliente = new Cliente();
	}
	
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<Credito> getCreditos() {
		return creditos;
	}

	public void setCreditos(List<Credito> creditos) {
		this.creditos = creditos;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
}
