package com.dto.integration.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

public class CreditNoteDTO {

	private static final long serialVersionUID = 1L;
	
	private String recordType;
	private BigInteger ncr_id;
	private BigDecimal total;
	private BigDecimal subtotal;
	private String tipoComprobante;
	private BigInteger folioNC;
	private String serieFolio;
	private String formaPago;
	private String metodoPago;
	private Date fecha;
	private BigDecimal descuento;
	private String regimen;
	private String codPosE;
	private String rfcE;
	private String nombreE;
	private String coloniaE;
	private String domicilioE;
	private String localidadE;
	private String ciudadE;
	private String estadoE;
	private String paisE;
	private String nombreC;
	private String rfcC;
	private String coloniaC;
	private String domicilioC;
	private String codPosC;
	private String ciudadC;
	private String estadoC;
	private String paisC;
	private String localidadC;
	private String numIntC;
	private String numExtC;
	private String moneda;
	private String letra;
	private BigInteger folioOrig;
	private String usoCfdi;
	private String comentario;
	private String clienteEmail;
	private BigDecimal tipoCambio;
	private int  numCliente;
	private String uuidFactura;
	private double sumIva002_0;
	private double sumIva002_16;
	private double sumIeps003_0;
	private double sumIeps003_06;
	private double sumIeps003_07;
	private double sumIeps003_09;
	private double sumBase002_16; //field 61
	private double sumBase002_0;  //field 66
	private double sumBase003_06; //field 71
	private double sumBase003_07; //field 76
	private double sumBase003_09;  //field 81
	private String sucursal;
	
	private List<CreditNoteDetailDTO> detail;
	
	public String getRecordType() {
		return recordType;
	}
	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}
	public BigInteger getFolioNC() {
		return folioNC;
	}
	public void setFolioNC(BigInteger folioNC) {
		this.folioNC = folioNC;
	}
	public BigDecimal getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}
	public String getFormaPago() {
		return formaPago;
	}
	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}
	public String getMetodoPago() {
		return metodoPago;
	}
	public void setMetodoPago(String metodoPago) {
		this.metodoPago = metodoPago;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public BigDecimal getDescuento() {
		return descuento;
	}
	public void setDescuento(BigDecimal descuento) {
		this.descuento = descuento;
	}
	public String getRegimen() {
		return regimen;
	}
	public void setRegimen(String regimen) {
		this.regimen = regimen;
	}
	public String getCodPosE() {
		return codPosE;
	}
	public void setCodPosE(String codPosE) {
		this.codPosE = codPosE;
	}
	public String getRfcE() {
		return rfcE;
	}
	public void setRfcE(String rfcE) {
		this.rfcE = rfcE;
	}
	public String getNombreE() {
		return nombreE;
	}
	public void setNombreE(String nombreE) {
		this.nombreE = nombreE;
	}
	public String getColoniaE() {
		return coloniaE;
	}
	public void setColoniaE(String coloniaE) {
		this.coloniaE = coloniaE;
	}
	public String getDomicilioE() {
		return domicilioE;
	}
	public void setDomicilioE(String domicilioE) {
		this.domicilioE = domicilioE;
	}
	public String getLocalidadE() {
		return localidadE;
	}
	public void setLocalidadE(String localidadE) {
		this.localidadE = localidadE;
	}
	public String getCiudadE() {
		return ciudadE;
	}
	public void setCiudadE(String ciudadE) {
		this.ciudadE = ciudadE;
	}
	public String getEstadoE() {
		return estadoE;
	}
	public void setEstadoE(String estadoE) {
		this.estadoE = estadoE;
	}
	public String getPaisE() {
		return paisE;
	}
	public void setPaisE(String paisE) {
		this.paisE = paisE;
	}
	public String getNombreC() {
		return nombreC;
	}
	public void setNombreC(String nombreC) {
		this.nombreC = nombreC;
	}
	public String getRfcC() {
		return rfcC;
	}
	public void setRfcC(String rfcC) {
		this.rfcC = rfcC;
	}
	public String getColoniaC() {
		return coloniaC;
	}
	public void setColoniaC(String coloniaC) {
		this.coloniaC = coloniaC;
	}
	public String getDomicilioC() {
		return domicilioC;
	}
	public void setDomicilioC(String domicilioC) {
		this.domicilioC = domicilioC;
	}
	public String getCodPosC() {
		return codPosC;
	}
	public void setCodPosC(String codPosC) {
		this.codPosC = codPosC;
	}
	public String getCiudadC() {
		return ciudadC;
	}
	public void setCiudadC(String ciudadC) {
		this.ciudadC = ciudadC;
	}
	public String getEstadoC() {
		return estadoC;
	}
	public void setEstadoC(String estadoC) {
		this.estadoC = estadoC;
	}
	public String getPaisC() {
		return paisC;
	}
	public void setPaisC(String paisC) {
		this.paisC = paisC;
	}
	public String getLocalidadC() {
		return localidadC;
	}
	public void setLocalidadC(String localidadC) {
		this.localidadC = localidadC;
	}
	public String getLetra() {
		return letra;
	}
	public void setLetra(String letra) {
		this.letra = letra;
	}
	public BigInteger getFolioOrig() {
		return folioOrig;
	}
	public void setFolioOrig(BigInteger folioOrig) {
		this.folioOrig = folioOrig;
	}
	public String getSerieFolio() {
		return serieFolio;
	}
	public void setSerieFolio(String serieFolio) {
		this.serieFolio = serieFolio;
	}
	public String getUsoCfdi() {
		return usoCfdi;
	}
	public void setUsoCfdi(String usoCfdi) {
		this.usoCfdi = usoCfdi;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public List<CreditNoteDetailDTO> getDetail() {
		return detail;
	}
	public void setDetail(List<CreditNoteDetailDTO> detail) {
		this.detail = detail;
	}
	public BigInteger getNcr_id() {
		return ncr_id;
	}
	public void setNcr_id(BigInteger ncr_id) {
		this.ncr_id = ncr_id;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public String getTipoComprobante() {
		return tipoComprobante;
	}
	public void setTipoComprobante(String tipoComprobante) {
		this.tipoComprobante = tipoComprobante;
	}
	public String getNumExtC() {
		return numExtC;
	}
	public void setNumExtC(String numExtC) {
		this.numExtC = numExtC;
	}
	public String getNumIntC() {
		return numIntC;
	}
	public void setNumIntC(String numIntC) {
		this.numIntC = numIntC;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	
	public String getClienteEmail() {
		return clienteEmail;
	}
	public void setClienteEmail(String clienteEmail) {
		this.clienteEmail = clienteEmail;
	}
	public BigDecimal getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(BigDecimal tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public String getUuidFactura() {
		return uuidFactura;
	}
	public void setUuidFactura(String uuidFactura) {
		this.uuidFactura = uuidFactura;
	}
	public double getSumIva002_16() {
		return sumIva002_16;
	}
	public void setSumIva002_16(double sumIva002_16) {
		this.sumIva002_16 = sumIva002_16;
	}
	public double getSumIeps003_0() {
		return sumIeps003_0;
	}
	public void setSumIeps003_0(double sumIeps003_0) {
		this.sumIeps003_0 = sumIeps003_0;
	}
	public double getSumIeps003_06() {
		return sumIeps003_06;
	}
	public void setSumIeps003_06(double sumIeps003_06) {
		this.sumIeps003_06 = sumIeps003_06;
	}
	public double getSumIeps003_07() {
		return sumIeps003_07;
	}
	public void setSumIeps003_07(double sumIeps003_07) {
		this.sumIeps003_07 = sumIeps003_07;
	}
	public double getSumIeps003_09() {
		return sumIeps003_09;
	}
	public void setSumIeps003_09(double sumIeps003_09) {
		this.sumIeps003_09 = sumIeps003_09;
	}
	public double getSumIva002_0() {
		return sumIva002_0;
	}
	public void setSumIva002_0(double sumIva002_0) {
		this.sumIva002_0 = sumIva002_0;
	}
	public double getSumBase002_16() {
		return sumBase002_16;
	}
	public void setSumBase002_16(double sumBase002_16) {
		this.sumBase002_16 = sumBase002_16;
	}
	public double getSumBase002_0() {
		return sumBase002_0;
	}
	public void setSumBase002_0(double sumBase002_0) {
		this.sumBase002_0 = sumBase002_0;
	}
	public double getSumBase003_06() {
		return sumBase003_06;
	}
	public void setSumBase003_06(double sumBase003_06) {
		this.sumBase003_06 = sumBase003_06;
	}
	public double getSumBase003_07() {
		return sumBase003_07;
	}
	public void setSumBase003_07(double sumBase003_07) {
		this.sumBase003_07 = sumBase003_07;
	}
	public double getSumBase003_09() {
		return sumBase003_09;
	}
	public void setSumBase003_09(double sumBase003_09) {
		this.sumBase003_09 = sumBase003_09;
	}
	public int getNumCliente() {
		return numCliente;
	}
	public void setNumCliente(int numCliente) {
		this.numCliente = numCliente;
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}

	
}
