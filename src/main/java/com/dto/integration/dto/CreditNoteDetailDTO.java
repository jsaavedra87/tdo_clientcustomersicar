package com.dto.integration.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

public class CreditNoteDetailDTO {
	
	private String detailType;
	private BigInteger ncr_id;
	private BigInteger folioNc;
	private BigDecimal importe;
	private BigDecimal importeConImpuesto;
	private BigDecimal precioUnitario;
	private int numLinea;
	private String descripcion;
	private String unidadMedida;
	private String clave;
	private BigDecimal tipoCambio;
	private BigDecimal cantidad;
	private String nombreAduana;
	private String numerodocAduanero;
	private Date fechaPedimento;
	private String claveProdServ;
	private String claveUnidad;
	private String tipoImpuestoTrasladado;
	private BigDecimal baseImpuestoTrasladado;
	private BigDecimal tasaImpuestoTrasladado;
	private BigDecimal importeImpuestoTrasladado;
	
	public String getDetailType() {
		return detailType;
	}
	public void setDetailType(String detailType) {
		this.detailType = detailType;
	}
	public BigInteger getFolioNc() {
		return folioNc;
	}
	public void setFolioNc(BigInteger folioNc) {
		this.folioNc = folioNc;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public BigDecimal getCantidad() {
		return cantidad;
	}
	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}
	public String getNombreAduana() {
		return nombreAduana;
	}
	public void setNombreAduana(String nombreAduana) {
		this.nombreAduana = nombreAduana;
	}
	public String getNumerodocAduanero() {
		return numerodocAduanero;
	}
	public void setNumerodocAduanero(String numerodocAduanero) {
		this.numerodocAduanero = numerodocAduanero;
	}
	public String getClaveProdServ() {
		return claveProdServ;
	}
	public void setClaveProdServ(String claveProdServ) {
		this.claveProdServ = claveProdServ;
	}
	public String getClaveUnidad() {
		return claveUnidad;
	}
	public void setClaveUnidad(String claveUnidad) {
		this.claveUnidad = claveUnidad;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public BigInteger getNcr_id() {
		return ncr_id;
	}
	public void setNcr_id(BigInteger ncr_id) {
		this.ncr_id = ncr_id;
	}
	public int getNumLinea() {
		return numLinea;
	}
	public void setNumLinea(int numLinea) {
		this.numLinea = numLinea;
	}
	public BigDecimal getImporte() {
		return importe;
	}
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}
	public Date getFechaPedimento() {
		return fechaPedimento;
	}
	public void setFechaPedimento(Date fechaPedimento) {
		this.fechaPedimento = fechaPedimento;
	}
	public String getTipoImpuestoTrasladado() {
		return tipoImpuestoTrasladado;
	}
	public void setTipoImpuestoTrasladado(String tipoImpuestoTrasladado) {
		this.tipoImpuestoTrasladado = tipoImpuestoTrasladado;
	}
	public BigDecimal getTasaImpuestoTrasladado() {
		return tasaImpuestoTrasladado;
	}
	public void setTasaImpuestoTrasladado(BigDecimal tasaImpuestoTrasladado) {
		this.tasaImpuestoTrasladado = tasaImpuestoTrasladado;
	}
	public BigDecimal getBaseImpuestoTrasladado() {
		return baseImpuestoTrasladado;
	}
	public void setBaseImpuestoTrasladado(BigDecimal baseImpuestoTrasladado) {
		this.baseImpuestoTrasladado = baseImpuestoTrasladado;
	}
	public BigDecimal getImporteImpuestoTrasladado() {
		return importeImpuestoTrasladado;
	}
	public void setImporteImpuestoTrasladado(BigDecimal importeImpuestoTrasladado) {
		this.importeImpuestoTrasladado = importeImpuestoTrasladado;
	}
	public String getUnidadMedida() {
		return unidadMedida;
	}
	public void setUnidadMedida(String unidadMedida) {
		this.unidadMedida = unidadMedida;
	}
	public BigDecimal getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(BigDecimal tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public BigDecimal getPrecioUnitario() {
		return precioUnitario;
	}
	public void setPrecioUnitario(BigDecimal precioUnitario) {
		this.precioUnitario = precioUnitario;
	}
	public BigDecimal getImporteConImpuesto() {
		return importeConImpuesto;
	}
	public void setImporteConImpuesto(BigDecimal importeConImpuesto) {
		this.importeConImpuesto = importeConImpuesto;
	}
	
	

}
