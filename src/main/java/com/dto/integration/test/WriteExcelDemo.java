package com.dto.integration.test;

import java.awt.Component;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class WriteExcelDemo
{
    public static void main(String[] args)
    {
        //Blank workbook
        XSSFWorkbook workbook = new XSSFWorkbook();
         
        //Create a blank sheet
        XSSFSheet sheet = workbook.createSheet("Employee Data");
          
        //This data needs to be written (Object[])
        Map<String, Object[]> data = new TreeMap<String, Object[]>();
        data.put("1", new Object[] {"ID", "NAME", "LASTNAME"});
        data.put("2", new Object[] {1, "Amit", "Shukla"});
        data.put("3", new Object[] {2, "Lokesh", "Gupta"});
        data.put("4", new Object[] {3, "John", "Adwards"});
        data.put("5", new Object[] {4, "Brian", "Schultz"});
          
        //Iterate over data and write to sheet
        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset)
        {
            Row row = sheet.createRow(rownum++);
            Object [] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr)
            {
               Cell cell = row.createCell(cellnum++);
               if(obj instanceof String)
                    cell.setCellValue((String)obj);
                else if(obj instanceof Integer)
                    cell.setCellValue((Integer)obj);
            }
        }
        try
        {
            
        	FileOutputStream out = null;
        	String defaultPath = new JFileChooser().getFileSystemView().getDefaultDirectory().toString();
        	JFileChooser jfc = new JFileChooser();
        	
        	FileNameExtensionFilter filter = new FileNameExtensionFilter("xlsx", "xls");
    		jfc.setDialogTitle("Guardar datos del cultivo");
    		jfc.setFileFilter(filter);
    		jfc.setSelectedFile(new File(defaultPath + "\\" + "cultivos.xlsx"));
    		
    		int returnValue = jfc.showDialog(null, "Guardar archivo");
    		if (returnValue == JFileChooser.APPROVE_OPTION) {
    			 String path = jfc.getSelectedFile().getAbsolutePath();
    			 out = new FileOutputStream(new File(path));
    	         workbook.write(out);
    	            out.close();
    		}
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
