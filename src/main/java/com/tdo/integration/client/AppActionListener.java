package com.tdo.integration.client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.dto.integration.dto.CreditDTO;
import com.dto.integration.dto.CreditNoteDTO;
import com.dto.integration.dto.SicarCustomerDto;
import com.tdo.integration.client.model.CreditNoteRequestEntity;
import com.tdo.integration.client.model.CustomerAdditionalSpecs;
import com.tdo.integration.client.model.SicarProductPrice;
import com.tdo.integration.client.model.Sucursal;
import com.tdo.integration.client.model.UDC;
import com.tdo.integration.services.ClientService;
import com.tdo.integration.services.CreditLimitsService;
import com.tdo.integration.services.SicarService;
import com.tdo.integration.ui.CreditLimitsWindow;
import com.tdo.integration.ui.CreditNoteDetailFrame;
import com.tdo.integration.ui.FrameManager;
import com.tdo.integration.util.AppConstants;

public class AppActionListener implements ActionListener {

	Properties props;
	
	@Autowired
	SicarService sicarService;
	
	@Autowired
	ClientService clientService;
	
	@Autowired
	CreditLimitsService creditLimitsService;
	
	private JTable custTable;
	private JTable itemTable;
	private JTextField searchField;
	private JTextField searchItemField;
	private JTable credNotesTable;
	Sucursal sucursal;

	static Logger logger = Logger.getLogger(AppActionListener.class);
	
	@SuppressWarnings("resource")
	public void actionPerformed(ActionEvent e) {
		String command = e.getActionCommand();
		ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Module.xml");
		FrameManager mf = (FrameManager)context.getBean("mainFrame");
		AppActionListener bl = (AppActionListener)context.getBean("appActionListener");

		if(command.equals("EXIT")) {
			int input = JOptionPane.showConfirmDialog(null, "Desea cerrar la aplicaci�n?");
			if (input == 0) {
				System.exit(WindowConstants.EXIT_ON_CLOSE);
			}
		}
		sucursal = clientService.getBranch();
		if(sucursal != null) {
			if (command.equals("SEARCH_CUSTOMER")) {
				searchCustomer();
			}else if (command.equals("DOWNLOAD_RECORDS")) {
				exportRecords();
			}
		}else {
			if (command.equals("SAVE_BRANCH")) {
				saveBranch();
			}else {
				JOptionPane.showMessageDialog(null, "Debe de registrar una sucursal antes de ejecutar alguna acci�n.");
			}
		}
		
		if("OPEN_HISTORIC_PRICES".equals(command)) {
			JFrame exchangeRateFrame = mf.itemPriceFrame(bl);
			exchangeRateFrame.setVisible(true);
		}
		
		if("OPEN_CULTIVO_ITEM".equals(command)) {
			SpecsWindow sw = (SpecsWindow)context.getBean("specsWindow");
			sw.setProperties(props);
			mf.cultivoManagerFrame(bl, sw);		
		}
		
		if (command.equals("SEARCH_CUST")) {
			searchCustomer();
		}
		if (command.equals("SEARCH_ITEM")) {
			searchItem();
		}
		
		if("OPEN_ITEM_PRICE".equals(command)) {
			 mf.itemPriceFrame(bl).setVisible(true);
		}
		
		if("OPEN_ITEM_CREDIT".equals(command)) {					
			CreditLimitsWindow clw = (CreditLimitsWindow)context.getBean("creditLimitsWindow");
			clw.setProperties(props);			
			bl.setProperties(props);
			mf.setProperties(props);
			mf.creditLimitManagerFrame(bl, clw);
		}
		if("OPEN_ITEM_NC".equals(command)) {					
			CreditNoteDetailFrame ncW = (CreditNoteDetailFrame)context.getBean("creditNotesWindow");
			ncW.setProperties(props);			
			bl.setProperties(props);
			mf.setProperties(props);
			mf.creditNoteMainFrame(bl, ncW);
		}
		
		
		if("SEARCH_CUST_CREDIT".equals(command)) {
			logger.info("Inicia b�squeda del cliente...");
			searchCreditLimits();
		}
		
		if (command.equals("SEARCH_NC")) {
			searchCreditNotes();
		}
		
	}

	public void setProperties(Properties props) {
		this.props = props;
	}
	
	
	private void searchCustomer() {
		try {
			if(!"".equals(searchField.getText())) {
				
				boolean hasRecords = false;
				List<Integer> custList = clientService.getCustomerWithSpecsList();
				if(custList != null) {
					hasRecords = true;
				}
				
				DefaultTableModel model = (DefaultTableModel)custTable.getModel();
				model.setRowCount(0);
				List<SicarCustomerDto> list = sicarService.listCustomers(searchField.getText().trim());
				if(list != null) {
					for(SicarCustomerDto o : list) {
						String v = "";
						if(hasRecords) {
							if(custList.contains(o.getCustomerId())) {
								v = "SI";
							}
						}
						model.addRow(new Object[]{o.getCustomerId(), o.getCustomerName(), o.getEmail(), o.getAddress(), o.getPhone(), v});
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void searchItem() {
	  try {
		if(!"".equals(searchItemField.getText())) {
			
			DefaultTableModel model = (DefaultTableModel)itemTable.getModel();
			model.setRowCount(0);
			List<SicarProductPrice> list = clientService.getSicarProductDtoListByItem(searchItemField.getText().trim());
			if(list != null) {
				for(SicarProductPrice s : list) {
					model.addRow(new Object[]{s.getItemIdentifier(),s.getDescription(),s.getExchangeRate(),s.getCurrency(),s.getPurchasePriceAvrg(), s.getUpdatedDate(), s.getId()});
				}
			}
		}else {
			JOptionPane.showMessageDialog(null, "Ingrese un valor");
		}
	  }catch(Exception e){
		e.printStackTrace();
	}
			
	}
	
	private void searchCreditLimits() {
		try {
			if(!"".equals(searchField.getText())) {
				logger.info("Se busca cliente \"" + searchField.getText() + "\"");
				DefaultTableModel model = (DefaultTableModel)custTable.getModel();
				model.setRowCount(0);				
				List<CreditDTO> list = creditLimitsService.getCustomerCredit(props.getProperty("restCustomerCredit"), searchField.getText().trim());
								
				if(list != null) {
					logger.info("Se obtuvieron " + list.size() + " resultados de la b�squeda.");
					for(CreditDTO o : list) {
						model.addRow(new Object[]{o.getCliente().getClienteNubeId(), o.getCliente().getClienteNombre(), o.getCliente().getClienteRFC(), o.getCliente().getClienteCURP(), o.getCliente().getClienteLimiteAbsoluto()});
					}
				} else {
					logger.info("La lista de clientes es nula.");
				}				
			}			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Ocurri� un error al realizar la b�squeda. ", e);
		}
	}
	
	public void searchCreditNotes() {
		try {
//			if(!"".equals(searchField.getText())) {			
				DefaultTableModel model = (DefaultTableModel)credNotesTable.getModel();
				model.setRowCount(0);
				List<CreditNoteDTO> list = sicarService.listCreditNotes(searchField.getText().trim());
				if(list != null) {
					for(CreditNoteDTO o : list) {
						CreditNoteRequestEntity credNoteRequest = clientService.getCreditNoteRequestByFolio(o);
						String statusText= renderStatusHelper(credNoteRequest);
						String errorCode ="";
						String errorDesc ="";
						if(credNoteRequest!=null) {
							errorCode= credNoteRequest.getCode();
							errorDesc = credNoteRequest.getDesc();
						}
						model.addRow(new Object[]{o.getFolioNC(), o.getNombreC(), o.getRfcC(), o.getTotal(), o.getFecha(),statusText,errorCode,errorDesc});
					}
				}
//			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public String renderStatusHelper(CreditNoteRequestEntity creditNoteRequest) {
		
		String statusRV=AppConstants.SIN_FACTURAR;
		if(creditNoteRequest!=null) {
		
		UDC udc = clientService.getUdcByUdcKey(creditNoteRequest.getStatus(),AppConstants.NCSTATUSES);
			if (udc!=null) {
				statusRV = udc.getUdcValue();
			}
		}
		
		return statusRV;
	}
	
	public Map<String,String> getAllStatuses() {
		Map<String,String> statusesMap = new HashMap<String,String>();
		List<UDC> statuses = clientService.getUdcBySystem(AppConstants.NCSTATUSES);
		if (statuses!=null) {
			for (UDC udc : statuses) {
				statusesMap.put(udc.getUdcValue(),udc.getUdcKey());
  		     }
	     }
		return statusesMap;
	}
	
	
	public void setCustomerTable(JTable custTable) {
		this.custTable = custTable;
	}
	
	public void setItemTable(JTable itemTable) {
		this.itemTable = itemTable;
	}
	
	public JTextField setCurrentExchangeRate() {
		JTextField txtField = new JTextField();
		txtField.setBorder(null);
		txtField.setBackground(null);
		/*
		List<UDC>tCambioList =clientService.getUdcBySystem("TCAMBIO");
		txtField.setText(txtField.getText() + tCambioList.get(0).getUdcValue().toString());
		*/
		txtField.setText("");
		return txtField;
		
	}
	public void setCustomerSearchField(JTextField searchField) {
		this.searchField = searchField;
	}
	
	public void setItemField(JTextField searchItemField) {
		this.searchItemField = searchItemField;
	}
	
	private  void saveBranch() {
		String value = JOptionPane.showInputDialog("Sucursal:");
		if(value != null) {
			if(!"".equals(value)) {
				Sucursal s = new Sucursal();
				s.setId(0);
				s.setSucursal(value);
				clientService.saveBranch(s);
				JOptionPane.showMessageDialog(null, "La nueva sucursal ha sido registrada.");
			}else {
				JOptionPane.showMessageDialog(null, "No registr� nig�n valor");
			}
		}
	}

	public void setCreditNotesTables(JTable credNotesTable) {
		this.credNotesTable = credNotesTable;
	}
	
	
	@SuppressWarnings("resource")
	private void exportRecords() {
		
        List<CustomerAdditionalSpecs> list = clientService.getAllCustomerSpecs();
        if(list != null) {
        	if(list.size() > 0) {
                 XSSFWorkbook workbook = new XSSFWorkbook();
                 XSSFSheet sheet = workbook.createSheet("Datos");
        		 Map<String, Object[]> data = new TreeMap<String, Object[]>();
        		    data.put("1", new Object[] {"Linea" , "Cliente", "Tipo Cultivo", "Forma Cultivo", "Direcci�n de Cultivo", "Superficie M2", "Estado", "Municipio", "Localidad", "Sucursal", "Email", "Tel�fono"});
        		 	int i = 2;
        		    for(CustomerAdditionalSpecs o : list) {
            	        data.put(String.valueOf(i), new Object[] {i, o.getCustomerName(), o.getTipoCultivo(), o.getFormaCultivo(), o.getDireccionCultivo(), o.getSuperficie(), o.getEstado(), o.getMunicipio(), o.getLocalidad(), o.getSucursal(), o.getCustomerEmail(), o.getCustomerPhone()});
            	        i = i + 1;
        		 	}

        	        Set<String> keyset = data.keySet();
        	        int rownum = 0;
        	        for (String key : keyset)
        	        {
        	            Row row = sheet.createRow(rownum++);
        	            Object [] objArr = data.get(key);
        	            int cellnum = 0;
        	            for (Object obj : objArr)
        	            {
        	               Cell cell = row.createCell(cellnum++);
        	               if(obj instanceof String)
        	                    cell.setCellValue((String)obj);
        	                else if(obj instanceof Integer)
        	                    cell.setCellValue((Integer)obj);
        	                else if(obj instanceof Double)
        	                    cell.setCellValue((Double)obj);
        	            }
        	        }
        	        try
        	        {
        	            
        	        	FileOutputStream out = null;
        	        	String defaultPath = new JFileChooser().getFileSystemView().getDefaultDirectory().toString();
        	        	JFileChooser jfc = new JFileChooser();
        	        	
        	        	FileNameExtensionFilter filter = new FileNameExtensionFilter("xlsx", "xls");
        	    		jfc.setDialogTitle("Guardar datos del cultivo");
        	    		jfc.setFileFilter(filter);
        	    		jfc.setSelectedFile(new File(defaultPath + "\\" + "cultivos.xlsx"));
        	    		
        	    		int returnValue = jfc.showDialog(null, "Guardar archivo");
        	    		if (returnValue == JFileChooser.APPROVE_OPTION) {
        	    			 String path = jfc.getSelectedFile().getAbsolutePath();
        	    			 out = new FileOutputStream(new File(path));
        	    	         workbook.write(out);
        	    	         out.close();
        	    	         JOptionPane.showMessageDialog(null, "Los datos fueron exportados de forma exitosa");
        	    		}
        	        }
        	        catch (Exception e)
        	        {
        	            e.printStackTrace();
        	            JOptionPane.showMessageDialog(null, "Ocurri� un error durante la exportaci�n: " + e.getMessage());
        	        }
        	}
        }
          
       
    }
	
}
