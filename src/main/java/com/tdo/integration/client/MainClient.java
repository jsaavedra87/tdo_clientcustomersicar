package com.tdo.integration.client;

import java.awt.EventQueue;
import java.util.Properties;

import javax.swing.JFrame;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.tdo.integration.ui.FrameManager;

public class MainClient {

//	private JFrame frame;
//	private static JMenuBar mb;
//  private static JMenu menu1;
//  private static JMenuItem mi1;

	static Logger logger = Logger.getLogger(MainClient.class);
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@SuppressWarnings("resource")
			public void run() {
				try {				
					//App Boostrap					
					logger.info(":::::::::::::::::::::::::: INICIO APLICACIÓN ::::::::::::::::::::::::::");
					ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Module.xml");
					AppActionListener bl = (AppActionListener)context.getBean("appActionListener");
					SpecsWindow sw = (SpecsWindow)context.getBean("specsWindow");
					FrameManager mf = (FrameManager)context.getBean("mainFrame");
					Properties props = new Properties();
					props.load(MainClient.class.getResourceAsStream("/app.properties"));
					//RestConnection.initConnection(props);
					mf.setProperties(props);
					sw.setProperties(props);
					bl.setProperties(props);
					JFrame mainMenu=mf.mainFrame(bl);
					mainMenu.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}


