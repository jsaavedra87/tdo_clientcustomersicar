package com.tdo.integration.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.Format;
import java.text.NumberFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

import org.springframework.beans.factory.annotation.Autowired;

import com.tdo.integration.client.model.CustomerAdditionalSpecs;
import com.tdo.integration.client.model.Sucursal;
import com.tdo.integration.client.model.UDC;
import com.tdo.integration.services.ClientService;
import com.tdo.integration.services.SicarService;

public class SpecsWindow {
	
	Properties props;
	
	@Autowired
	SicarService sicarService;
	
	@Autowired
	ClientService clientService;

	static JTextField numCliente = new JTextField("");
	static JTextField nombreCliente = new JTextField("");
	
	static JTextField recordId = new JTextField("");
	static //static JTextField tipoCultivo = new JTextField("");
	Format nbr = NumberFormat.getNumberInstance();
	static JFormattedTextField superficie =  new JFormattedTextField(nbr);
	//static JTextField formaCultivo = new JTextField("");
	static JTextArea direccionCultivo = new JTextArea();
	static JTextField distritoRiego = new JTextField("");
	
	static JComboBox<String> tipoCultivo = new JComboBox<String>();
	static JComboBox<String> formaCultivo = new JComboBox<String>();
	
	static JComboBox<String> estados = new JComboBox<String>();
	static JComboBox<String> municipios = new JComboBox<String>();
	static JComboBox<String> localidades = new JComboBox<String>();
	
	static JTextField seq = new JTextField("");
	static JTextField codCli = new JTextField("");
	static JTextField supUom = new JTextField("");
	static JTextField activo = new JTextField("");
	static JTextField enviado = new JTextField("");
	static JTextField fechaReg = new JTextField("");
	
	private JMenuBar mb;
    private JMenu menu1;
    private JMenuItem mi1,mi2;
	
	String[] columnNames = { "id", "Secuencia", "Id cliente", "Codigo cliente", "Nombre cliente",
			                 "Tipo de Cultivo", "Superficie", "SuperficieUOM", "Forma de cultivo",
			                 "Direccion cultivo", "Distrito riego", "Activo", "Enviado", "Fecha Registro", 
			                 "Estado", "Municipio","Localidad"};
	
	DefaultTableModel model = new DefaultTableModel(columnNames, 0);
	JTable specsDtlTable  = new JTable(model);
		
	public JFrame getSpecsPanel() {
		
		 JFrame frame = new JFrame("Informaci�n del cultivo");
		 frame.setSize(1000, 440);
		 frame.getContentPane().setBackground(Color.WHITE);
		 frame.setLocationRelativeTo(null); 
		 try 
         {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
         } catch (Exception e) {
            e.printStackTrace();
         }
		 
		    mb=new JMenuBar();
	        frame.setJMenuBar(mb);
	        menu1=new JMenu("Opciones");
	        mb.add(menu1);
	        mi1=new JMenuItem("Nuevo Cultivo");
	        mi1.addActionListener(new ActionListener() {
		        public void actionPerformed(ActionEvent e) {
		        	nuevoCultivo(true);
		        }
		    });
	        menu1.add(mi1);
	        mi2=new JMenuItem("Nueva Forma de Cultivo");
	        mi2.addActionListener(new ActionListener() {
		        public void actionPerformed(ActionEvent e) {
		        	nuevaFormaCultivo(true);
		        }
		    });
	        menu1.add(mi2);

		    // HEADER PANEL
		    JPanel headerPanel = new JPanel();
		    Font fnt = new Font("SansSerif", Font.BOLD, 20);
		    headerPanel.setBackground(Color.WHITE);
		    
		    numCliente.setBorder(new LineBorder(Color.white,1)); 
		    numCliente.setPreferredSize(new Dimension(30, 25));
		    numCliente.setEditable(false);
		    numCliente.setFont(fnt);
		    numCliente.setBackground(Color.WHITE);
		    
		    nombreCliente.setBorder(new LineBorder(Color.white,1)); 
		    nombreCliente.setPreferredSize(new Dimension(700, 25));
		    nombreCliente.setEditable(false);
		    nombreCliente.setFont(fnt);
		    nombreCliente.setBackground(Color.WHITE);
		    
		    headerPanel.add(numCliente);
		    headerPanel.add(new JLabel("  -   "));
		    headerPanel.add(nombreCliente);
			
		    // LEFT PANEL
			JPanel leftPanel = new JPanel();
			leftPanel.setBackground(Color.WHITE);
			leftPanel.setLayout(new GridBagLayout());
			
			GridBagConstraints constraints = new GridBagConstraints();
			constraints.insets = new Insets(5, 5, 5, 5);
			
			recordId.setVisible(false);
			seq.setVisible(false);
			codCli.setVisible(false);
			supUom.setVisible(false);
			activo.setVisible(false);
			enviado.setVisible(false);
			fechaReg.setVisible(false);
			
			constraints.gridx = 0; 
			constraints.gridy = 0; 
			leftPanel.add(new JLabel("Tipo de Cultivo:"), constraints);
			
			tipoCultivo.setPreferredSize(new Dimension(300, 29));
			constraints.gridx = 1;
			constraints.gridy = 0; 
			leftPanel.add(tipoCultivo, constraints);
			
			constraints.gridx = 0; 
			constraints.gridy = 1; 
			leftPanel.add(new JLabel("Superficie (m2):"), constraints);
			
			superficie.setPreferredSize(new Dimension(300, 29));
			constraints.gridx = 1;
			constraints.gridy = 1; 
			leftPanel.add(superficie, constraints);
			
			constraints.gridx = 0; 
			constraints.gridy = 2; 
			leftPanel.add(new JLabel("Forma de cultivo:"), constraints);
			
			formaCultivo.setPreferredSize(new Dimension(300, 29));
			constraints.gridx = 1;
			constraints.gridy = 2;
			leftPanel.add(formaCultivo, constraints);
			
			constraints.gridx = 0; 
			constraints.gridy = 3; 
			leftPanel.add(new JLabel("Direcci�n:"), constraints);

			direccionCultivo.setPreferredSize(new Dimension(300, 50));
			constraints.gridx = 1;
			constraints.gridy = 3; 
			JScrollPane panelDir = new JScrollPane(direccionCultivo);
			leftPanel.add(panelDir, constraints);

			/*
			constraints.gridx = 0; 
			constraints.gridy = 4; 
			leftPanel.add(new JLabel("Distrito de riego:"), constraints);
			
			distritoRiego.setPreferredSize(new Dimension(300, 29));
			constraints.gridx = 1;
			constraints.gridy = 4; 
			leftPanel.add(distritoRiego, constraints);
			*/
			
			constraints.gridx = 0; 
			constraints.gridy = 5; 
			leftPanel.add(new JLabel("Estado:"), constraints);
			
			estados.setPreferredSize(new Dimension(300, 29));
			constraints.gridx = 1;
			constraints.gridy = 5; 
			leftPanel.add(estados, constraints);
			
			constraints.gridx = 0; 
			constraints.gridy = 6; 
			leftPanel.add(new JLabel("Municipio:"), constraints);
			
			municipios.setPreferredSize(new Dimension(300, 29));
			constraints.gridx = 1;
			constraints.gridy = 6; 
			leftPanel.add(municipios, constraints);
			
			constraints.gridx = 0; 
			constraints.gridy = 7; 
			leftPanel.add(new JLabel("Localidad:"), constraints);
			
			localidades.setPreferredSize(new Dimension(300, 29));
			constraints.gridx = 1;
			constraints.gridy = 7; 
			leftPanel.add(localidades, constraints);
						
			JButton saveButton = new JButton("Guardar registro");
			saveButton.addActionListener(new ActionListener() {
		        public void actionPerformed(ActionEvent e) {
		        	updateRecord(true);
		        }
		    });
			
			JButton deleteButton = new JButton("Eliminar registro");
			deleteButton.addActionListener(new ActionListener() {
		        public void actionPerformed(ActionEvent e) {
		        	updateRecord(false);
		        }
		    });
			
			JButton newButton = new JButton("Nuevo registro");
			newButton.addActionListener(new ActionListener() {
		        public void actionPerformed(ActionEvent e) {
		        	newRecord();
		        }
		    });

			JPanel buttonsPanel = new JPanel();
			buttonsPanel.setBackground(Color.WHITE);
			buttonsPanel.add(saveButton);
			buttonsPanel.add(deleteButton);
			buttonsPanel.add(newButton);
			
			constraints.gridx = 1;
			constraints.gridy = 8; 
			leftPanel.add(buttonsPanel, constraints);
			
			
			// RIGHT PANEL
			specsDtlTable.setFocusable(false);
			specsDtlTable.setRowSelectionAllowed(true);
			specsDtlTable.setPreferredSize(new Dimension(900, 180));
			specsDtlTable.setBounds(10, 10, 10, 10); 
			JScrollPane sp = new JScrollPane(specsDtlTable,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			specsDtlTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
			sp.setBackground(Color.WHITE);
			
			specsDtlTable.addMouseListener(new java.awt.event.MouseAdapter() {
			    @Override
			    public void mouseClicked(java.awt.event.MouseEvent evt) {
			    	try {
			        recordId.setText(specsDtlTable.getModel().getValueAt(specsDtlTable.getSelectedRow(),0).toString());
			        seq.setText(specsDtlTable.getModel().getValueAt(specsDtlTable.getSelectedRow(),1).toString());
			        numCliente.setText(specsDtlTable.getModel().getValueAt(specsDtlTable.getSelectedRow(),2).toString());
			        codCli.setText(specsDtlTable.getModel().getValueAt(specsDtlTable.getSelectedRow(),3).toString());
			        nombreCliente.setText(specsDtlTable.getModel().getValueAt(specsDtlTable.getSelectedRow(),4).toString());
			       // tipoCultivo.setText(specsDtlTable.getModel().getValueAt(specsDtlTable.getSelectedRow(),5).toString());
			        tipoCultivo.setSelectedItem(specsDtlTable.getModel().getValueAt(specsDtlTable.getSelectedRow(),5).toString());
			        superficie.setValue(new Double(specsDtlTable.getModel().getValueAt(specsDtlTable.getSelectedRow(),6).toString()));
			        supUom.setText(specsDtlTable.getModel().getValueAt(specsDtlTable.getSelectedRow(),7).toString());
			        //formaCultivo.setText(specsDtlTable.getModel().getValueAt(specsDtlTable.getSelectedRow(),8).toString());
			        formaCultivo.setSelectedItem(specsDtlTable.getModel().getValueAt(specsDtlTable.getSelectedRow(),8).toString());
			        direccionCultivo.setText(specsDtlTable.getModel().getValueAt(specsDtlTable.getSelectedRow(),9).toString());
			        distritoRiego.setText(specsDtlTable.getModel().getValueAt(specsDtlTable.getSelectedRow(),10).toString());
			        activo.setText(specsDtlTable.getModel().getValueAt(specsDtlTable.getSelectedRow(),11).toString());
			        enviado.setText(specsDtlTable.getModel().getValueAt(specsDtlTable.getSelectedRow(),12).toString());
			        fechaReg.setText(specsDtlTable.getModel().getValueAt(specsDtlTable.getSelectedRow(),13).toString());
			        estados.setSelectedItem(specsDtlTable.getModel().getValueAt(specsDtlTable.getSelectedRow(),14).toString());
			        municipios.setSelectedItem(specsDtlTable.getModel().getValueAt(specsDtlTable.getSelectedRow(),15).toString());
			        localidades.setSelectedItem(specsDtlTable.getModel().getValueAt(specsDtlTable.getSelectedRow(),16).toString());
			    	}catch(Exception e) {
			    		
			    	}
			    }
			});
				
		 sp.setPreferredSize(new Dimension(520, 300));
		 frame.getContentPane().add(BorderLayout.NORTH, headerPanel);
		 frame.getContentPane().add(BorderLayout.WEST, leftPanel);
		 frame.getContentPane().add(BorderLayout.EAST, sp);
		 return frame;
		
	}
	
	protected boolean nuevaFormaCultivo(boolean b) {
		String value = JOptionPane.showInputDialog("Forma de Cultivo:");
		if(value != null) {
			if(!"".equals(value)) {
				UDC udc = new UDC();
				udc.setId(0);
				udc.setUdcKey("NUEVAFORMACULTIVO");
				udc.setUdcSystem("FORMACULTIVO");
				udc.setUdcValue(value);
				List<UDC> currentList = clientService.getUdcBySystem("FORMACULTIVO");
				for(UDC o : currentList) {
					int i = o.getUdcValue().trim().compareToIgnoreCase(value.trim());
					if(i == 0) {
						JOptionPane.showMessageDialog(null, "La forma de cultivo ya existe:" + o.getUdcValue().trim() +". Intente registrar otra forma de cultivo.");
						return false;
					}
				}
				clientService.saveUDC(udc);
				JOptionPane.showMessageDialog(null, "La nueva forma cultivo ha sido registrada. Cierre la ventana y vuelva a abrirla para actualizar la lista.");
				return true;
			}else {
				JOptionPane.showMessageDialog(null, "No registr� nig�n valor");
				return false;
			}
		}else {
			return false;
		}
	}

	protected boolean nuevoCultivo(boolean b) {
		String value = JOptionPane.showInputDialog("Nombre del Cultivo:");
		if(value != null) {
			if(!"".equals(value)) {
				UDC udc = new UDC();
				udc.setId(0);
				udc.setUdcKey("NUEVOCULTIVO");
				udc.setUdcSystem("CULTIVO");
				udc.setUdcValue(value);
				List<UDC> currentList = clientService.getUdcBySystem("CULTIVO");
				for(UDC o : currentList) {
					int i = o.getUdcValue().trim().compareToIgnoreCase(value.trim());
					if(i == 0) {
						JOptionPane.showMessageDialog(null, "El tipo de cultivo ya existe:" + o.getUdcValue().trim() +". Intente registrar otro tipo de cultivo.");
						return false;
					}
				}
				clientService.saveUDC(udc);
				JOptionPane.showMessageDialog(null, "El nuevo cultivo ha sido registrado. Cierre la ventana y vuelva a abrirla para actualizar la lista.");
				return true;
			}else {
				JOptionPane.showMessageDialog(null, "No registr� nig�n valor");
				return false;
			}
		}else {
			return false;
		}
	}

	public boolean updateRecord(boolean enable) {
		try {

			Object tc = tipoCultivo.getSelectedItem();
			if(tc == null) {
				 JOptionPane.showMessageDialog(null, "Debe seleccionar un tipo de cultivo");
			}else {
				
				int records = clientService.getTotalRecords(Integer.valueOf(numCliente.getText()));
				if(records <= 4) {
					CustomerAdditionalSpecs specs = new CustomerAdditionalSpecs();
					if(!"".equals(recordId.getText())) {
						specs.setId(Integer.valueOf(recordId.getText()));
					}
					specs.setCustomerId(Integer.valueOf(numCliente.getText()));
					specs.setCustomerName(nombreCliente.getText());
					specs.setCustomerCode("");
					specs.setTipoCultivo(tipoCultivo.getSelectedItem().toString());

					if(!"".equals(superficie.getText())) {
						Object sup = superficie.getValue();
						if (sup instanceof Double) {
							specs.setSuperficie(new Double(((Double) sup).doubleValue()));	
						}else if (sup instanceof Integer) {
							specs.setSuperficie(new Double(((Integer) sup).intValue()));	
						} else if (sup instanceof Number) {
							specs.setSuperficie(new Double(((Number) sup).intValue()));
					    }
					}else {
						specs.setSuperficie(0.0);
					}

					specs.setSuperficieUnidadMedida("");
					specs.setFormaCultivo(formaCultivo.getSelectedItem().toString());
					specs.setDireccionCultivo(direccionCultivo.getText());
					specs.setDistritoRiego(distritoRiego.getText());
					specs.setEstado(estados.getSelectedItem().toString());
					specs.setMunicipio(municipios.getSelectedItem().toString());
					specs.setLocalidad(localidades.getSelectedItem().toString());
					specs.setEnabled(enable);
					specs.setRecordDate(new Date());
					specs.setSent(false);
					
					Sucursal s = clientService.getBranch();
					Properties props = new Properties();
					props.load(SpecsWindow.class.getResourceAsStream("/app.properties"));
					if(s != null) {
						clientService.saveRecord(specs, props.getProperty("restCustomerSpecs"),s.getSucursal());
					}else {
						JOptionPane.showMessageDialog(null, "Debe de registrar una sucursal antes de enviar la informaci�n.");
						return false;
					}

					DefaultTableModel model = (DefaultTableModel)specsDtlTable.getModel();
					model.setRowCount(0);
					
					List<CustomerAdditionalSpecs> list = clientService.getCustomerSpecsList(Integer.valueOf(numCliente.getText()));
					if(list != null) {
						for(CustomerAdditionalSpecs o : list) {
							model.addRow(new Object[]{o.getId(), o.getSequence(), o.getCustomerId(), o.getCustomerCode(),
									                 o.getCustomerName(), o.getTipoCultivo(), o.getSuperficie(), 
									                 o.getSuperficieUnidadMedida(), o.getFormaCultivo(), o.getDireccionCultivo(),
									                 o.getDistritoRiego(), o.isEnabled(), o.isSent(), o.getRecordDate().toString(),
									                 o.getEstado(), o.getMunicipio(), o.getLocalidad()});
						}
					}
					newRecord();
				}else {
					JOptionPane.showMessageDialog(null, "No se permiten m�s de 5 registros por cliente");
				}
			}

		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return true;
	}
	
	public void newRecord() {
		recordId.setText("");
		seq.setText("");
		codCli.setText("");
		supUom.setText("");
		activo.setText("");
		enviado.setText("");
		fechaReg.setText("");
		tipoCultivo.setSelectedIndex(-1);
		superficie.setText("");
		formaCultivo.setSelectedIndex(-1);
		direccionCultivo.setText("");
		distritoRiego.setText("");
		estados.setSelectedIndex(-1);
		municipios.removeAllItems();
		localidades.removeAllItems();
	}
	
	public void setHeader(int customerId, String customerName) {		
		newRecord();
		numCliente.setText(String.valueOf(customerId));
		nombreCliente.setText(String.valueOf(customerName));

		
		tipoCultivo.removeAllItems();
		List<UDC> udcs = clientService.getUdcBySystem("CULTIVO");
		if(udcs != null) {
			for(UDC o : udcs) {
				tipoCultivo.addItem(o.getUdcValue());
			}
		}
		
    	municipios.removeAllItems();
		localidades.removeAllItems();
		
		ItemListener estadosListener = new ItemListener() {
		      public void itemStateChanged(ItemEvent itemEvent) {
		        int state = itemEvent.getStateChange();
		        if(state == ItemEvent.SELECTED) {
		        	String item = itemEvent.getItem().toString();
		        	List<String> list = clientService.getMunicipios(item);
		        	municipios.removeAllItems();
		    		localidades.removeAllItems();
		    		if(list != null) {
		    			for(String o : list) {
		    				municipios.addItem(o);
		    			}
		    		}
		        }

		      }
		    };
		    
		ItemListener municipiosListener = new ItemListener() {
		      public void itemStateChanged(ItemEvent itemEvent) {
		        int state = itemEvent.getStateChange();
		        if(state == ItemEvent.SELECTED) {
		        	String item = itemEvent.getItem().toString();
		        	List<String> list = clientService.getLocalidades(item);
		        	localidades.removeAllItems();
		    		if(list != null) {
		    			for(String o : list) {
		    				localidades.addItem(o);
		    			}
		    		}
		        }

		      }
		    };
		
		
	    municipios.addItemListener(municipiosListener);	 
		estados.addItemListener(estadosListener);
		List<String> str = clientService.getEstados();
		if(str != null) {
			for(String o : str) {
				estados.addItem(o);
			}
		}

		formaCultivo.removeAllItems();
		udcs = clientService.getUdcBySystem("FORMACULTIVO");
		if(udcs != null) {
			for(UDC o : udcs) {
				formaCultivo.addItem(o.getUdcValue());
			}
		}

		DefaultTableModel model = (DefaultTableModel)specsDtlTable.getModel();
		model.setRowCount(0);
		List<CustomerAdditionalSpecs> list = clientService.getCustomerSpecsList(customerId);
		if(list != null) {
			for(CustomerAdditionalSpecs o : list) {
				model.addRow(new Object[]{o.getId(), o.getSequence(), o.getCustomerId(), o.getCustomerCode(),
						                 o.getCustomerName(), o.getTipoCultivo(), o.getSuperficie(), 
						                 o.getSuperficieUnidadMedida(), o.getFormaCultivo(), o.getDireccionCultivo(),
						                 o.getDistritoRiego(), o.isEnabled(), o.isSent(), o.getRecordDate().toString(),
						                 o.getEstado(), o.getMunicipio(), o.getLocalidad()});
			}
		}
		
       try {
	
		specsDtlTable.getColumnModel().getColumn(5).setPreferredWidth(350);
		specsDtlTable.getColumnModel().getColumn(6).setPreferredWidth(350);
		specsDtlTable.getColumnModel().getColumn(8).setPreferredWidth(350);
		specsDtlTable.getColumnModel().getColumn(9).setPreferredWidth(490);
		specsDtlTable.getColumnModel().getColumn(14).setPreferredWidth(300);
		specsDtlTable.getColumnModel().getColumn(15).setPreferredWidth(350);
		specsDtlTable.getColumnModel().getColumn(16).setPreferredWidth(350);
		
				
		specsDtlTable.getColumnModel().getColumn(0).setMinWidth(0);
		specsDtlTable.getColumnModel().getColumn(0).setMaxWidth(0);
		specsDtlTable.getColumnModel().getColumn(0).setWidth(0);
		
		specsDtlTable.getColumnModel().getColumn(1).setMinWidth(0);
		specsDtlTable.getColumnModel().getColumn(1).setMaxWidth(0);
		specsDtlTable.getColumnModel().getColumn(1).setWidth(0);
		
		specsDtlTable.getColumnModel().getColumn(2).setMinWidth(0);
		specsDtlTable.getColumnModel().getColumn(2).setMaxWidth(0);
		specsDtlTable.getColumnModel().getColumn(2).setWidth(0);
		
		specsDtlTable.getColumnModel().getColumn(3).setMinWidth(0);
		specsDtlTable.getColumnModel().getColumn(3).setMaxWidth(0);
		specsDtlTable.getColumnModel().getColumn(3).setWidth(0);
		
		specsDtlTable.getColumnModel().getColumn(4).setMinWidth(0);
		specsDtlTable.getColumnModel().getColumn(4).setMaxWidth(0);
		specsDtlTable.getColumnModel().getColumn(4).setWidth(0);

		specsDtlTable.getColumnModel().getColumn(7).setMinWidth(0);
		specsDtlTable.getColumnModel().getColumn(7).setMaxWidth(0);
		specsDtlTable.getColumnModel().getColumn(7).setWidth(0);
		
		specsDtlTable.getColumnModel().getColumn(10).setMinWidth(0);
		specsDtlTable.getColumnModel().getColumn(10).setMaxWidth(0);
		specsDtlTable.getColumnModel().getColumn(10).setWidth(0);
		
		specsDtlTable.getColumnModel().getColumn(11).setMinWidth(0);
		specsDtlTable.getColumnModel().getColumn(11).setMaxWidth(0);
		specsDtlTable.getColumnModel().getColumn(11).setWidth(0);
		
		specsDtlTable.getColumnModel().getColumn(12).setMinWidth(0);
		specsDtlTable.getColumnModel().getColumn(12).setMaxWidth(0);
		specsDtlTable.getColumnModel().getColumn(12).setWidth(0);
		
		specsDtlTable.getColumnModel().getColumn(13).setMinWidth(0);
		specsDtlTable.getColumnModel().getColumn(13).setMaxWidth(0);
		specsDtlTable.getColumnModel().getColumn(13).setWidth(0);

       }catch(Exception e) {
    	   
       }

	}
	
	public void setProperties(Properties props) {
		this.props = props;
	}
	
}
