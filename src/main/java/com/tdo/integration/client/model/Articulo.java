package com.tdo.integration.client.model;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;


public class Articulo {

	private static final long serialVersionUID = 1L;
	private int id;
	private BigInteger idVenta;	
	private Integer articuloId;
	private String articuloClave;
	private String articuloDescripcion;
	private BigDecimal articuloPrecio1;
	private BigDecimal articuloPrecio2;
	private BigDecimal articuloPrecio3;
	private BigDecimal ventaDetalleCantidad;
	private String ventaDetalleUnidad;
	private BigDecimal ventaDetallePrecioSinIva;
	private BigDecimal ventaDetallePrecioConIva;
	private BigDecimal ventaDetalleImporteSinIva;
	private BigDecimal ventaDetalleImporteConIva;
	private BigInteger notaCreditoId;
	private BigInteger folioNC;
	private BigDecimal descuentoTotal;
	private BigDecimal precioCompra;
	private BigDecimal importeCompra;
	private String userDef1;
	private Date userDef2;
	private Boolean lote;
	
	//JAvila: Obtiene el tipo para identifiacr que es de tipo Kit
	private Integer tipo;
	private Integer paquete;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public BigInteger getIdVenta() {
		return idVenta;
	}
	public void setIdVenta(BigInteger idVenta) {
		this.idVenta = idVenta;
	}
	public Integer getArticuloId() {
		return articuloId;
	}
	public void setArticuloId(Integer articuloId) {
		this.articuloId = articuloId;
	}
	public String getArticuloClave() {
		return articuloClave;
	}
	public void setArticuloClave(String articuloClave) {
		this.articuloClave = articuloClave;
	}
	public String getArticuloDescripcion() {
		return articuloDescripcion;
	}
	public void setArticuloDescripcion(String articuloDescripcion) {
		this.articuloDescripcion = articuloDescripcion;
	}
	public BigDecimal getArticuloPrecio1() {
		return articuloPrecio1;
	}
	public void setArticuloPrecio1(BigDecimal articuloPrecio1) {
		this.articuloPrecio1 = articuloPrecio1;
	}
	public BigDecimal getArticuloPrecio2() {
		return articuloPrecio2;
	}
	public void setArticuloPrecio2(BigDecimal articuloPrecio2) {
		this.articuloPrecio2 = articuloPrecio2;
	}
	public BigDecimal getArticuloPrecio3() {
		return articuloPrecio3;
	}
	public void setArticuloPrecio3(BigDecimal articuloPrecio3) {
		this.articuloPrecio3 = articuloPrecio3;
	}
	public BigDecimal getVentaDetalleCantidad() {
		return ventaDetalleCantidad;
	}
	public void setVentaDetalleCantidad(BigDecimal ventaDetalleCantidad) {
		this.ventaDetalleCantidad = ventaDetalleCantidad;
	}
	public String getVentaDetalleUnidad() {
		return ventaDetalleUnidad;
	}
	public void setVentaDetalleUnidad(String ventaDetalleUnidad) {
		this.ventaDetalleUnidad = ventaDetalleUnidad;
	}
	public BigDecimal getVentaDetallePrecioSinIva() {
		return ventaDetallePrecioSinIva;
	}
	public void setVentaDetallePrecioSinIva(BigDecimal ventaDetallePrecioSinIva) {
		this.ventaDetallePrecioSinIva = ventaDetallePrecioSinIva;
	}
	public BigDecimal getVentaDetallePrecioConIva() {
		return ventaDetallePrecioConIva;
	}
	public void setVentaDetallePrecioConIva(BigDecimal ventaDetallePrecioConIva) {
		this.ventaDetallePrecioConIva = ventaDetallePrecioConIva;
	}
	public BigDecimal getVentaDetalleImporteSinIva() {
		return ventaDetalleImporteSinIva;
	}
	public void setVentaDetalleImporteSinIva(BigDecimal ventaDetalleImporteSinIva) {
		this.ventaDetalleImporteSinIva = ventaDetalleImporteSinIva;
	}
	public BigDecimal getVentaDetalleImporteConIva() {
		return ventaDetalleImporteConIva;
	}
	public void setVentaDetalleImporteConIva(BigDecimal ventaDetalleImporteConIva) {
		this.ventaDetalleImporteConIva = ventaDetalleImporteConIva;
	}
	public BigInteger getNotaCreditoId() {
		return notaCreditoId;
	}
	public void setNotaCreditoId(BigInteger notaCreditoId) {
		this.notaCreditoId = notaCreditoId;
	}	
	public BigInteger getFolioNC() {
		return folioNC;
	}
	public void setFolioNC(BigInteger folioNC) {
		this.folioNC = folioNC;
	}
	public BigDecimal getDescuentoTotal() {
		return descuentoTotal;
	}
	public void setDescuentoTotal(BigDecimal descuentoTotal) {
		this.descuentoTotal = descuentoTotal;
	}
	public BigDecimal getPrecioCompra() {
		return precioCompra;
	}
	public void setPrecioCompra(BigDecimal precioCompra) {
		this.precioCompra = precioCompra;
	}
	public BigDecimal getImporteCompra() {
		return importeCompra;
	}
	public void setImporteCompra(BigDecimal importeCompra) {
		this.importeCompra = importeCompra;
	}
	public String getUserDef1() {
		return userDef1;
	}
	public void setUserDef1(String userDef1) {
		this.userDef1 = userDef1;
	}
	public Date getUserDef2() {
		return userDef2;
	}
	public void setUserDef2(Date userDef2) {
		this.userDef2 = userDef2;
	}
	public Boolean getLote() {
		return lote;
	}
	public void setLote(Boolean lote) {
		this.lote = lote;
	}
	public Integer getTipo() {
		return tipo;
	}
	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}
	public Integer getPaquete() {
		return paquete;
	}
	public void setPaquete(Integer paquete) {
		this.paquete = paquete;
	}

	
	

}
