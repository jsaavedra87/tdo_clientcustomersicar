package com.tdo.integration.client.model;

import java.math.BigDecimal;
import java.math.BigInteger;

public class Cliente {
	
	private static final long serialVersionUID = 1L;
	private int id;
	private BigInteger clienteNubeId;
	private Integer clienteId;	
	private BigInteger idVenta;	
	private String clienteNombre;	
	private String clienteRFC;
	private String clienteCURP;
	private String clienteDomicilio;
	private String clienteColonia;
	private String clienteNoExt;
	private String clienteNoInt;
	private String clienteLocalidad;
	private String clienteCiudad;
	private String clienteEstado;
	private String clientePais;
	private String clienteCodigoPostal;
	private BigDecimal clienteLimite;
	private BigDecimal clienteLimiteAbsoluto;
	private Integer clienteDiasCredito;
	private String clienteClave;	
	private Integer clientePrecio;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}	
	public BigInteger getClienteNubeId() {
		return clienteNubeId;
	}
	public void setClienteNubeId(BigInteger clienteNubeId) {
		this.clienteNubeId = clienteNubeId;
	}
	public Integer getClienteId() {
		return clienteId;
	}
	public void setClienteId(Integer clienteId) {
		this.clienteId = clienteId;
	}
	public BigInteger getIdVenta() {
		return idVenta;
	}
	public void setIdVenta(BigInteger idVenta) {
		this.idVenta = idVenta;
	}
	public String getClienteNombre() {
		return clienteNombre;
	}
	public void setClienteNombre(String clienteNombre) {
		this.clienteNombre = clienteNombre;
	}
	public String getClienteRFC() {
		return clienteRFC;
	}
	public void setClienteRFC(String clienteRFC) {
		this.clienteRFC = clienteRFC;
	}
	public String getClienteCURP() {
		return clienteCURP;
	}
	public void setClienteCURP(String clienteCURP) {
		this.clienteCURP = clienteCURP;
	}
	public String getClienteDomicilio() {
		return clienteDomicilio;
	}
	public void setClienteDomicilio(String clienteDomicilio) {
		this.clienteDomicilio = clienteDomicilio;
	}
	public String getClienteColonia() {
		return clienteColonia;
	}
	public void setClienteColonia(String clienteColonia) {
		this.clienteColonia = clienteColonia;
	}
	public String getClienteNoExt() {
		return clienteNoExt;
	}
	public void setClienteNoExt(String clienteNoExt) {
		this.clienteNoExt = clienteNoExt;
	}
	public String getClienteNoInt() {
		return clienteNoInt;
	}
	public void setClienteNoInt(String clienteNoInt) {
		this.clienteNoInt = clienteNoInt;
	}
	public String getClienteLocalidad() {
		return clienteLocalidad;
	}
	public void setClienteLocalidad(String clienteLocalidad) {
		this.clienteLocalidad = clienteLocalidad;
	}
	public String getClienteCiudad() {
		return clienteCiudad;
	}
	public void setClienteCiudad(String clienteCiudad) {
		this.clienteCiudad = clienteCiudad;
	}
	public String getClienteEstado() {
		return clienteEstado;
	}
	public void setClienteEstado(String clienteEstado) {
		this.clienteEstado = clienteEstado;
	}
	public String getClientePais() {
		return clientePais;
	}
	public void setClientePais(String clientePais) {
		this.clientePais = clientePais;
	}
	public String getClienteCodigoPostal() {
		return clienteCodigoPostal;
	}
	public void setClienteCodigoPostal(String clienteCodigoPostal) {
		this.clienteCodigoPostal = clienteCodigoPostal;
	}
	public BigDecimal getClienteLimite() {
		return clienteLimite;
	}
	public void setClienteLimite(BigDecimal clienteLimite) {
		this.clienteLimite = clienteLimite;
	}	
	public BigDecimal getClienteLimiteAbsoluto() {
		return clienteLimiteAbsoluto;
	}
	public void setClienteLimiteAbsoluto(BigDecimal clienteLimiteAbsoluto) {
		this.clienteLimiteAbsoluto = clienteLimiteAbsoluto;
	}
	public Integer getClienteDiasCredito() {
		return clienteDiasCredito;
	}
	public void setClienteDiasCredito(Integer clienteDiasCredito) {
		this.clienteDiasCredito = clienteDiasCredito;
	}
	public String getClienteClave() {
		return clienteClave;
	}
	public void setClienteClave(String clienteClave) {
		this.clienteClave = clienteClave;
	}
	public Integer getClientePrecio() {
		return clientePrecio;
	}
	public void setClientePrecio(Integer clientePrecio) {
		this.clientePrecio = clientePrecio;
	}
	
}
