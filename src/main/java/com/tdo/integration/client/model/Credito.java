package com.tdo.integration.client.model;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

public class Credito {
	
	private static final long serialVersionUID = 1L;
	private int id;
	private String sucursal;
	private BigInteger creditoId;
	private Integer clienteId;
	private BigInteger clienteNubeId;
	private BigInteger idVenta;
	private Date fechaLimite;
	private BigDecimal total;
	private String comentario;
	private Integer status;
	private List<Abono> abono;
	private List<NotaCreditoAbono> ncAbono;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}	
	public BigInteger getCreditoId() {
		return creditoId;
	}
	public void setCreditoId(BigInteger creditoId) {
		this.creditoId = creditoId;
	}	
	public Integer getClienteId() {
		return clienteId;
	}
	public void setClienteId(Integer clienteId) {
		this.clienteId = clienteId;
	}
	public BigInteger getClienteNubeId() {
		return clienteNubeId;
	}
	public void setClienteNubeId(BigInteger clienteNubeId) {
		this.clienteNubeId = clienteNubeId;
	}
	public BigInteger getIdVenta() {
		return idVenta;
	}
	public void setIdVenta(BigInteger idVenta) {
		this.idVenta = idVenta;
	}
	public Date getFechaLimite() {
		return fechaLimite;
	}
	public void setFechaLimite(Date fechaLimite) {
		this.fechaLimite = fechaLimite;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public List<Abono> getAbono() {
		return abono;
	}
	public void setAbono(List<Abono> abono) {
		this.abono = abono;
	}	
	public List<NotaCreditoAbono> getNcAbono() {
		return ncAbono;
	}
	public void setNcAbono(List<NotaCreditoAbono> ncAbono) {
		this.ncAbono = ncAbono;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
		
}
