package com.tdo.integration.client.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="CustomerAdditionalSpecs")
public class CustomerAdditionalSpecs {
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	private String sucursal;
	private int sequence;
	private int customerId;
	private String customerCode;
	private String customerName;
	private String customerAddress;
	private String customerEmail;
	private String customerPhone;
	private String tipoCultivo;
	private double superficie;
	private String superficieUnidadMedida;
	private String formaCultivo;
	private String direccionCultivo;
	private String distritoRiego;
	private boolean enabled;
	private boolean sent;
	private Date recordDate;
	private String estado;
	private String municipio;
	private String localidad;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getTipoCultivo() {
		return tipoCultivo;
	}
	public void setTipoCultivo(String tipoCultivo) {
		this.tipoCultivo = tipoCultivo;
	}
	public double getSuperficie() {
		return superficie;
	}
	public void setSuperficie(double superficie) {
		this.superficie = superficie;
	}
	public String getSuperficieUnidadMedida() {
		return superficieUnidadMedida;
	}
	public void setSuperficieUnidadMedida(String superficieUnidadMedida) {
		this.superficieUnidadMedida = superficieUnidadMedida;
	}
	public String getFormaCultivo() {
		return formaCultivo;
	}
	public void setFormaCultivo(String formaCultivo) {
		this.formaCultivo = formaCultivo;
	}
	public String getDireccionCultivo() {
		return direccionCultivo;
	}
	public void setDireccionCultivo(String direccionCultivo) {
		this.direccionCultivo = direccionCultivo;
	}
	public String getDistritoRiego() {
		return distritoRiego;
	}
	public void setDistritoRiego(String distritoRiego) {
		this.distritoRiego = distritoRiego;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public boolean isSent() {
		return sent;
	}
	public void setSent(boolean sent) {
		this.sent = sent;
	}
	public int getSequence() {
		return sequence;
	}
	public void setSequence(int sequence) {
		this.sequence = sequence;
	}
	public Date getRecordDate() {
		return recordDate;
	}
	public void setRecordDate(Date recordDate) {
		this.recordDate = recordDate;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getMunicipio() {
		return municipio;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public String getLocalidad() {
		return localidad;
	}
	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public String getCustomerEmail() {
		return customerEmail;
	}
	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}
	public String getCustomerPhone() {
		return customerPhone;
	}
	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}
	
	
}
