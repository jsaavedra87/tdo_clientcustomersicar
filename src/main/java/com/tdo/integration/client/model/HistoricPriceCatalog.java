package com.tdo.integration.client.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="HistoricPrices")
public class HistoricPriceCatalog {

	private static final long serialVersionUID = 1L;
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	private int version;
	private String itemIdentifier;
	private double exchangeRate;
	private String currency; 
	private double purchasePriceAvrg;
	private double profitPercentage1;
	private double productPrice1;
	private double profitPercentage2;
	private double productPrice2;
	private double profitPercentage3;
	private double productPrice3;
	private Date   transactionDate;
		
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public String getItemIdentifier() {
		return itemIdentifier;
	}
	public void setItemIdentifier(String itemIdentifier) {
		this.itemIdentifier = itemIdentifier;
	}
	public double getExchangeRate() {
		return exchangeRate;
	}
	public void setExchangeRate(double exchangeRate) {
		this.exchangeRate = exchangeRate;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public double getPurchasePriceAvrg() {
		return purchasePriceAvrg;
	}
	public void setPurchasePriceAvrg(double purchasePriceAvrg) {
		this.purchasePriceAvrg = purchasePriceAvrg;
	}
	public double getProfitPercentage1() {
		return profitPercentage1;
	}
	public void setProfitPercentage1(double profitPercentage1) {
		this.profitPercentage1 = profitPercentage1;
	}
	public double getProductPrice1() {
		return productPrice1;
	}
	public void setProductPrice1(double productPrice1) {
		this.productPrice1 = productPrice1;
	}
	public double getProfitPercentage2() {
		return profitPercentage2;
	}
	public void setProfitPercentage2(double profitPercentage2) {
		this.profitPercentage2 = profitPercentage2;
	}
	public double getProductPrice2() {
		return productPrice2;
	}
	public void setProductPrice2(double productPrice2) {
		this.productPrice2 = productPrice2;
	}
	public double getProfitPercentage3() {
		return profitPercentage3;
	}
	public void setProfitPercentage3(double profitPercentage3) {
		this.profitPercentage3 = profitPercentage3;
	}
	public double getProductPrice3() {
		return productPrice3;
	}
	public void setProductPrice3(double productPrice3) {
		this.productPrice3 = productPrice3;
	}
	public Date getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}		
	
}
