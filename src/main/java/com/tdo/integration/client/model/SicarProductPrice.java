package com.tdo.integration.client.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="sicarProductPrices")
public class SicarProductPrice {
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	//clave
	private String itemIdentifier;
	private String description;
	//tipo de cambio
	private double exchangeRate;
	//fixed value: USD 
	private String currency; 
	//precio de compra
	private double purchasePriceAvrg;
	private double profitPercentage1;
	private double productPrice1;
	private double profitPercentage2;
	private double productPrice2;
	private double profitPercentage3;
	private double productPrice3;
	private Date   updatedDate;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	// fixed values: Application or Integration
	private String updatedBy;
	
	public String getItemIdentifier() {
		return itemIdentifier;
	}
	public void setItemIdentifier(String itemIdentifier) {
		this.itemIdentifier = itemIdentifier;
	}
	public double getExchangeRate() {
		return exchangeRate;
	}
	public void setExchangeRate(double exchangeRate) {
		this.exchangeRate = exchangeRate;
	}
	public double getPurchasePriceAvrg() {
		return purchasePriceAvrg;
	}
	public void setPurchasePriceAvrg(double purchasePriceAvrg) {
		this.purchasePriceAvrg = purchasePriceAvrg;
	}
	public double getProfitPercentage1() {
		return profitPercentage1;
	}
	public void setProfitPercentage1(double profitPercentage1) {
		this.profitPercentage1 = profitPercentage1;
	}
	public double getProductPrice1() {
		return productPrice1;
	}
	public void setProductPrice1(double productPrice1) {
		this.productPrice1 = productPrice1;
	}
	public double getProfitPercentage2() {
		return profitPercentage2;
	}
	public void setProfitPercentage2(double profitPercentage2) {
		this.profitPercentage2 = profitPercentage2;
	}
	public double getProductPrice2() {
		return productPrice2;
	}
	public void setProductPrice2(double productPrice2) {
		this.productPrice2 = productPrice2;
	}
	public double getProfitPercentage3() {
		return profitPercentage3;
	}
	public void setProfitPercentage3(double profitPercentage3) {
		this.profitPercentage3 = profitPercentage3;
	}
	public double getProductPrice3() {
		return productPrice3;
	}
	public void setProductPrice3(double productPrice3) {
		this.productPrice3 = productPrice3;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		return "SicarProductPrice [id=" + id + ", itemIdentifier=" + itemIdentifier + ", description=" + description + 
				", exchangeRate=" + exchangeRate + ", currency=" + currency + ", purchasePriceAvrg=" + purchasePriceAvrg + 
				", profitPercentage1=" + profitPercentage1 + ", productPrice1=" + productPrice1 + ", profitPercentage2=" + profitPercentage2 + 
				", productPrice2=" + productPrice2 + ", profitPercentage3=" + profitPercentage3 + ", productPrice3=" + productPrice3 + 
				", updatedDate=" + updatedDate + "]";
	} 
	
}
