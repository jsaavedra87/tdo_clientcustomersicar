package com.tdo.integration.client.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="UDC")
public class UDC {
	
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	private String udcSystem;
	private String udcKey;
	private String udcValue;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUdcSystem() {
		return udcSystem;
	}
	public void setUdcSystem(String udcSystem) {
		this.udcSystem = udcSystem;
	}
	public String getUdcKey() {
		return udcKey;
	}
	public void setUdcKey(String udcKey) {
		this.udcKey = udcKey;
	}
	public String getUdcValue() {
		return udcValue;
	}
	public void setUdcValue(String udcValue) {
		this.udcValue = udcValue;
	}
	
	
}
