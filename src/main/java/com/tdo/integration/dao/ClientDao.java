package com.tdo.integration.dao;

import org.hibernate.transform.Transformers;
import org.hibernate.Query;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import javax.transaction.Transactional;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.tdo.integration.client.model.CreditNoteRequestEntity;
import com.tdo.integration.client.model.CustomerAdditionalSpecs;
import com.tdo.integration.client.model.HistoricPriceCatalog;
import com.tdo.integration.client.model.Localidades;
import com.tdo.integration.client.model.SicarProductPrice;
import com.tdo.integration.client.model.Sucursal;
import com.tdo.integration.client.model.UDC;

@Repository("clientDao")
@Transactional 
public class ClientDao {
	
	DataSource dataSource;
	Connection conn = null;
	
private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	public void saveRecord(CustomerAdditionalSpecs o) {
		Session session = this.sessionFactory.getCurrentSession();
		session.saveOrUpdate(o);
	}
	
	public void saveUDC(UDC o) {
		Session session = this.sessionFactory.getCurrentSession();
		session.saveOrUpdate(o);
	}
	
	public void saveBranch(Sucursal s) {
		Session session = this.sessionFactory.getCurrentSession();
		session.saveOrUpdate(s);
	}
	
	@SuppressWarnings("unchecked")
	public Sucursal getBranch() {
		Session session = this.sessionFactory.getCurrentSession();
		Sucursal s = null;
		Criteria cr = session.createCriteria(Sucursal.class);
		List<Sucursal> results = cr.list();
		if(results != null) {
			if(!results.isEmpty())
				s = results.get(0);
		}
		return s;
	}
	
	@SuppressWarnings("unchecked")
	public List<CustomerAdditionalSpecs> getCustomerSpecsList(int customerId) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(CustomerAdditionalSpecs.class);
		criteria.add(
				Restrictions.conjunction()
				.add(Restrictions.like("customerId", customerId))
				.add(Restrictions.eq("enabled", true))
				);
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Integer> getAllCustomerSpecsList() {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(CustomerAdditionalSpecs.class);
		criteria.setProjection(Projections.projectionList()
			    .add(Projections.groupProperty("customerId"))
			    );
		criteria.add(Restrictions.eq("enabled", true));
		return criteria.list();
	}
	
	
	@SuppressWarnings("unchecked")
	public List<CustomerAdditionalSpecs> getAllCustomerSpecs() {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(CustomerAdditionalSpecs.class);
		criteria.add(Restrictions.eq("enabled", true));
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<UDC> getUdcBySystem(String system) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(UDC.class);
		criteria.add(
				Restrictions.conjunction()
				.add(Restrictions.eq("udcSystem", system))
				);
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public UDC getUdcByUdcKeyAndSystem(String udckey, String system) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(UDC.class);
		criteria.add(
				Restrictions.conjunction()
				.add(Restrictions.eq("udcKey", udckey))
				.add(Restrictions.eq("udcSystem", system))
				);
		List<UDC> udcList = criteria.list();
		UDC udc =null;
		if(udcList!=null) {
			udc = udcList.get(0);
		}
		return udc;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<String> getEstados() {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Localidades.class);
		criteria.setProjection( Projections.distinct( Projections.property( "estado" ) ) );
		criteria.addOrder(Order.asc("estado"));
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getMunicipios(String estado) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Localidades.class);
		criteria.setProjection( Projections.distinct( Projections.property( "municipio" ) ) );
		criteria.add(
				Restrictions.conjunction()
				.add(Restrictions.eq("estado", estado))
				);
		criteria.addOrder(Order.asc("municipio"));
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getLocalidades(String municipio) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Localidades.class);
		criteria.setProjection( Projections.distinct( Projections.property( "localidad" ) ) );
		criteria.add(
				Restrictions.conjunction()
				.add(Restrictions.eq("municipio", municipio))
				);
		criteria.addOrder(Order.asc("localidad"));
		return criteria.list();
	}

	
	public void setDataSource(DataSource dataSource) {
		try {
			this.dataSource = dataSource;
			this.conn = dataSource.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public int getTotalRecords(int customerId){
		Session session = this.sessionFactory.getCurrentSession();
		Long count = (Long) session.createQuery("select count(*) from  CustomerAdditionalSpecs where customerId = " + customerId + " and enabled = true").uniqueResult();
		return count.intValue();
		
	}

	
	/**
	 * Method to persist the historic prices calculated daily according the
	 * exchangeRate
	 * 
	 * @param historicItemPrice
	 */
	public void saveHistoricPrice(List<HistoricPriceCatalog> historicItemPriceList) {
		Session session = this.sessionFactory.getCurrentSession();
		for (HistoricPriceCatalog historicPriceCatalog : historicItemPriceList) {

			session.save(historicPriceCatalog);
		}
	}

	public void saveProductsDto(List<SicarProductPrice> sicarProductList) {
		Session session = this.sessionFactory.getCurrentSession();
		for (SicarProductPrice sicarProductsDTO : sicarProductList) {
			 session.saveOrUpdate(sicarProductsDTO);
		}
	}

	@SuppressWarnings("unchecked")
	public List<SicarProductPrice> getSicarProductDtoListByItem(String clave) {
		
			Session session = sessionFactory.getCurrentSession();
			String sql = "select * from sicarproductprices where itemIdentifier  like"+ "'%"+ clave + "%'";

			try {
				Query query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(SicarProductPrice.class));
				return query.list();

			} catch (Exception e) {
				return null;
			}
	}
	
	@SuppressWarnings("unchecked")
	public List<SicarProductPrice> getSicarProductDtoList() {
		
			Session session = sessionFactory.getCurrentSession();
			String sql = "select * from sicarproductprices;";

			try {
				Query query = session.createSQLQuery(sql);
				query.setResultTransformer(Transformers.aliasToBean(SicarProductPrice.class));
				return query.list();

			} catch (Exception e) {
				return null;
			}
		}
	
	@SuppressWarnings("unchecked")
	public List<HistoricPriceCatalog> getHistoricPriceListByItem (String itemId){
		
		Session session = sessionFactory.getCurrentSession();
		String sql = "select * from HistoricPrices where itemIdentifier  like"+ "'%"+ itemId + "%'  order by transactionDate desc LIMIT 12";

		try {
			Query query = session.createSQLQuery(sql);
			query.setResultTransformer(Transformers.aliasToBean(HistoricPriceCatalog.class));
			return query.list();

		} catch (Exception e) {
			return null;
		}
	}
	
	public void saveCreditNoteRequest(CreditNoteRequestEntity creditNoteRequest) {
		Session session = this.sessionFactory.getCurrentSession();
		session.saveOrUpdate(creditNoteRequest);
	}
	
	@SuppressWarnings("unchecked")
	public CreditNoteRequestEntity getCreditNoteRequestByFolio (String folioNC){
		
		Session session = sessionFactory.getCurrentSession();
		String sql = "select * from CreditNoteRequest where folio="+ folioNC + "  order by date desc";

		try {
			Query query = session.createSQLQuery(sql);
			query.setResultTransformer(Transformers.aliasToBean(CreditNoteRequestEntity.class));
			 List<CreditNoteRequestEntity> results =query.list();
			 CreditNoteRequestEntity crn = null;
			 if(results != null) {
					if(!results.isEmpty())
						crn = results.get(0);
				}
			 return crn;

		} catch (Exception e) {
			return null;
		}
	}
	
}
