package com.tdo.integration.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;
import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dto.integration.dto.CreditNoteDTO;
import com.dto.integration.dto.CreditNoteDetailDTO;
import com.dto.integration.dto.SicarCustomerDto;
import com.dto.integration.dto.SicarItemDTO;
import com.tdo.integration.client.model.Articulo;
import com.tdo.integration.client.model.SicarProductPrice;
import com.tdo.integration.client.model.Sucursal;
import com.tdo.integration.services.SicarService;

@Repository("sicarDao")
@Transactional 
public class SicarDao {
	
	@Autowired
	ClientDao clientDao;
	
	DataSource dataSource;
	Connection conn = null;
    private SessionFactory sessionFactory;	
    static Logger logger = Logger.getLogger(SicarDao.class);
    
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@SuppressWarnings({ "unused", "unchecked" })
	public List<SicarCustomerDto> listCustomers(String name){
		
		try {
			Session session = sessionFactory.getCurrentSession();
			List<SicarCustomerDto> rows = null;
			String sql = "Select cli_id as customerId, nombre as customerName, CONCAT(domicilio, ' ', noExt, ' ', noInt, ', ', localidad, ' ', ciudad, ' ', estado, ', CP ', codigoPostal) as address, mail as email, telefono as phone From cliente where nombre like '%"
					+ name + "%' " + "order by nombre";
			SQLQuery query = session.createSQLQuery(sql);
			// query.addScalar("column1", Hibernate.LONG);
			query = session.createSQLQuery(sql);
			query.setResultTransformer(Transformers.aliasToBean(SicarCustomerDto.class));
			return query.list();
			
		}catch(Exception e) {
			return null;
		}
	}
	
	
	@SuppressWarnings({ "unused", "unchecked" })
	public SicarCustomerDto getCustomer(int id){
		
		try {
			Session session = sessionFactory.getCurrentSession();
			List<SicarCustomerDto> rows = null;
			String sql = "Select cli_id as customerId, nombre as customerName, CONCAT(domicilio, ' ', noExt, ' ', noInt, ', ', localidad, ' ', ciudad, ' ', estado, ', CP ', codigoPostal) as address, mail as email, telefono as phone From cliente where cli_id = "
					+ id;
			SQLQuery query = session.createSQLQuery(sql);
			// query.addScalar("column1", Hibernate.LONG);
			query = session.createSQLQuery(sql);
			query.setResultTransformer(Transformers.aliasToBean(SicarCustomerDto.class));
			List<SicarCustomerDto> list = query.list();
			if(list != null) {
				if(list.size() > 0) {
					return list.get(0);
				}else {
					return null;
				}
			}else {
				return null;
			}
			
		}catch(Exception e) {
			return null;
		}
	}
	
	public void setDataSource(DataSource dataSource) {
		try {
			this.dataSource = dataSource;
			this.conn = dataSource.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * get total records of Items.
	 * 
	 * @return
	 */
	public int getCountItemList() {

		Session session = sessionFactory.getCurrentSession();
		String sql = "SELECT COUNT(*) FROM articulo";

		try {
//			Integer count = (Integer) session.createQuery(sql).uniqueResult();
			Query query = session.createQuery(sql);
			query.uniqueResult();
			int count=0;
			final Object obj = query.uniqueResult();
            if (obj != null) {
               count = (int) obj;
            }
			return count;
		} catch (Exception e) {
			return 0;
		}
	}

	/**
	 * Persist current price in sicar db
	 * 
	 * @param item
	 */
	public void updateSicarItemPrice(List<SicarProductPrice> itemList) {

		try {

			Session session = this.sessionFactory.getCurrentSession();
			BigDecimal purchasePriceAvrg;
			String clave;
			String sql = "update articulo set precio1= :updatedPrice1, precio2= :updatedPrice2,precio3= :updatedPrice3, precioCompra= :purchasePriceAvrg,preCompraProm= :purchasePriceAvrg where clave=:clave";
			Query query = session.createSQLQuery(sql);
			for (SicarProductPrice hpc : itemList) {

				Articulo art = getArticulo(hpc.getItemIdentifier());
				
				if(art != null) {
					purchasePriceAvrg = BigDecimal.valueOf(hpc.getPurchasePriceAvrg()*hpc.getExchangeRate());
					        			
					double itemCost = Math. round(purchasePriceAvrg.doubleValue() * 1000.0) / 1000.0;;
					double m1 = art.getVentaDetallePrecioSinIva().doubleValue();
					double m2 = art.getVentaDetallePrecioConIva().doubleValue();
					double m3 = art.getVentaDetalleImporteSinIva().doubleValue();
					
					double p1 = ((m1/100) * itemCost) + itemCost;
					double p2 = ((m2/100) * itemCost) + itemCost;
					double p3 = ((m3/100) * itemCost) + itemCost;
							
					clave = hpc.getItemIdentifier();

					query.setParameter("updatedPrice1", p1);
					query.setParameter("updatedPrice2", p2);
					query.setParameter("updatedPrice3", p3);
					query.setParameter("purchasePriceAvrg", purchasePriceAvrg);
					query.setParameter("clave", clave);
					query.executeUpdate();
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public List<SicarItemDTO> getSicarItemListBytItemId(String itemId) {

		Session session = sessionFactory.getCurrentSession();
		String sql = "SELECT id,articuloClave as itemCode,articuloDescripcion as itemDescription ,articuloId as itemId,articuloPrecio1 as itemPrice FROM articulo where articuloId like '%"
				+ itemId + "%' ";

		try {
			SQLQuery query = session.createSQLQuery(sql);
			query = session.createSQLQuery(sql);
			query.setResultTransformer(Transformers.aliasToBean(SicarItemDTO.class));
			return query.list();

		} catch (Exception e) {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public Articulo getArticulo(String item) {
			Session session = sessionFactory.getCurrentSession();
			String sql = "SELECT e.art_id AS articuloId, " +
					"  e.clave AS articuloClave, " +
					"  e.descripcion AS articuloDescripcion, " +
					"  e.precio1 AS articuloPrecio1, " +
					"  e.precio2 AS articuloPrecio2, " +
					"  e.precio3 AS articuloPrecio3, " +
					"  e.margen1 AS ventaDetallePrecioSinIva, " +
					"  e.margen2 AS ventaDetallePrecioConIva, " +
					"  e.margen3 AS ventaDetalleImporteSinIva " +
					" FROM articulo e" +
					" WHERE e.clave = :item";
			
			SQLQuery query = session.createSQLQuery(sql);
			query = session.createSQLQuery(sql);
			query.setResultTransformer(Transformers.aliasToBean(Articulo.class));
			query.setParameter("item", item);
			List<Articulo> articulos = query.list();
			if(articulos != null) {
				if(articulos.size() > 0) {
					return articulos.get(0);
				}
			}
			
		return null;
	}
	
	public void updateTipoCambioIntoMoneda(double tc) {
		Session session = this.sessionFactory.getCurrentSession();
		String sql = "UPDATE MONEDA SET tipoCambio=:tc where abr='USD'";
		Query query = session.createSQLQuery(sql);
		query.setParameter("tc",tc);
		query.executeUpdate();
	}
	
	public void updateSicarNubeAct(List<SicarProductPrice> sicarProductList) {
		Session session = sessionFactory.getCurrentSession();
		 
		if(sicarProductList != null && !sicarProductList.isEmpty()) {
			for(SicarProductPrice sicarProduct : sicarProductList) {				
				try {
					String clave = sicarProduct.getItemIdentifier();				
					
					String sql = "SELECT IFNULL(art_id, 0) FROM articulo WHERE clave = :clave";
					Query qry = session.createSQLQuery(sql);
					qry.setParameter("clave", clave);
					BigInteger artId = (BigInteger) qry.uniqueResult();
					
					sql = "SELECT IFNULL(MAX(act_id), 0) FROM nubeact WHERE entidad = 'Inventario' AND id = :art_id";
					qry = session.createSQLQuery(sql);
					qry.setParameter("art_id", artId.intValue());
					BigInteger lastInvId = (BigInteger) qry.uniqueResult();
					
					sql = "SELECT IFNULL(MAX(act_id), 0) FROM nubeact WHERE entidad = 'Articulo' AND id = :art_id";
					qry = session.createSQLQuery(sql);
					qry.setParameter("art_id", artId.intValue());
					BigInteger lastArtId = (BigInteger) qry.uniqueResult();			
					
					sql = "UPDATE nubeact SET actualizado = 0 WHERE act_id IN (:act_id_inv, :act_id_art)";
					qry = session.createSQLQuery(sql);
					qry.setParameter("act_id_inv", lastInvId);
					qry.setParameter("act_id_art", lastArtId);
					qry.executeUpdate();	
					
				} catch (Exception e) {
					e.printStackTrace();
					logger.error("Ocurri� un Error al actualizar el registro en [nubeact], art�culo " + String.valueOf(sicarProduct.getItemIdentifier()), e);
					continue;
				}
			}
		}
	}

	@SuppressWarnings({ "unused", "unchecked" })
	public List<CreditNoteDTO> listCreditNotes(String param) {

		try {
			Session session = sessionFactory.getCurrentSession();
			List<CreditNoteDTO> rows = null;
			String criteria = "";
			if (param != null) {
				criteria = "AND (nombreC like '%" + param + "%'" + "or rfcC like '%" + param + "%') ";
			}
			String sql = "select folioNC, nombreC, rfcC, total, fecha from notacredito n "
					+ "JOIN detallen d ON n.ncr_id = d.ncr_id " + "where d.dv_ven_id IS NULL "
					+ "AND d.dv_art_id IS NULL " + criteria + "and folioNC is not null order by n.folioNC,nombreC";

			SQLQuery query = session.createSQLQuery(sql);
			query = session.createSQLQuery(sql);
			query.setResultTransformer(Transformers.aliasToBean(CreditNoteDTO.class));
			return query.list();

		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * method to prepare the credit note data to be send to EAI component
	 * 
	 * @param folioNC
	 * @return List<CreditNotesDTO>
	 */

	public List<CreditNoteDTO> getCreditNoteList(String param) {

		Session session = this.sessionFactory.getCurrentSession();

		
		Sucursal  sucursal = clientDao.getBranch();
		String sql = "SELECT n.ncr_id,n.folioNC,n.total,n.subtotal,n.formaPago,n.metodoPago,n.fecha,n.descuento,n.regimen,n.codPosE,n.rfcE,n.nombreE,n.coloniaE,n.domicilioE,n.localidadE,n.ciudadE,n.estadoE,n.paisE,n.nombreC,n.rfcC,n.coloniaC,n.domicilioC,n.codPosC,n.ciudadC,n.estadoC,n.paisC,n.localidadC,cl.noInt as numIntC,cl.noExt as numExtC,n.letra,cl.cli_id as numCliente,n.folioOrig,n.serieFolio,n.usoCfdi,n.monAbr as moneda, cl.mail as clienteEmail,ROUND(mon.tipoCambio,2) as tipoCambio,n.comentario,'' as uuidFactura "
				+ "from notacredito n " + "join cliente cl " + "on cl.cli_id = n.cli_id " + "join moneda mon "
				+ "on mon.abr = n.monAbr " + "JOIN detallen d ON n.ncr_id = d.ncr_id " + "WHERE d.dv_ven_id IS NULL "
				+ "AND d.dv_art_id IS NULL " + "AND n.folioNC =" + param + " order by n.folioNC";

		String sqlDtl = "SELECT a.ncr_id as ncr_id,a.folioNC AS folioNc,ROUND((d.importesin * a.total)/c.total,2) AS importe,ROUND((ROUND((SELECT (h.total * a.total)/c.total "
				+ "FROM detallevimpuesto h " + " WHERE d.ven_id = h.ven_id " + " AND d.art_id = h.art_id "
				+ " ORDER BY h.impuesto DESC "
				+ " LIMIT 1 ),2) + ROUND((d.importesin * a.total)/c.total,2)),2)  AS importeConImpuesto,"
				+ "				 replace(d.descripcion, ',', '') as descripcion,ROUND((d.preciosin * a.total)/c.total,2) AS precioUnitario,"
				+ " d.unidad as unidadMedida,d.clave AS clave, ROUND(a.monTipoCambio,2) AS tipoCambio,d.fechaDocAduanero AS fechaPedimento, "
				+ "				 ROUND(d.cantidad,2) AS cantidad,IFNULL(d.nombreAduana,'') as nombreAduana,IFNULL(d.numerodocAduanero,'') as numerodocAduanero,d.claveProdServ AS claveProdServ,   "
				+ "				   d.claveUnidad AS claveUnidad,   " + "		          (SELECT CASE    "
				+ "				 	WHEN f.nombre LIKE 'IEPS%'   THEN '003'    "
				+ "				 	WHEN f.nombre LIKE 'I.V.A.%' THEN '002'    " + "				 	END  "
				+ "				 	FROM detallevimpuesto f   " + "				    WHERE d.ven_id = f.ven_id    "
				+ "				    AND d.art_id = f.art_id    " + "				    ORDER BY f.impuesto DESC    "
				+ "				    LIMIT 1)	AS tipoImpuestoTrasladado,   "
				+ "				  ROUND((d.importesin * a.total)/c.total,2)	AS baseImpuestoTrasladado,   "
				+ "				  ROUND((SELECT (impuesto/100)    " + "				   FROM detallevimpuesto g   "
				+ "				   WHERE d.ven_id = g.ven_id   " + "				   AND d.art_id = g.art_id   "
				+ "				   ORDER BY g.impuesto DESC   "
				+ "				   LIMIT 1),6)	AS tasaImpuestoTrasladado,  "
				+ "				   ROUND((SELECT (h.total * a.total)/c.total   "
				+ "				 	FROM detallevimpuesto h    " + "				    WHERE d.ven_id = h.ven_id   "
				+ "				    AND d.art_id = h.art_id   " + "				    ORDER BY h.impuesto DESC   "
				+ "				    LIMIT 1 ),2) AS importeImpuestoTrasladado  "
				+ "				  FROM notacredito a   " + "                  JOIN detallen b "
				+ "				 ON a.ncr_id = b.ncr_id   " + "				  JOIN venta c   "
				+ "				 ON b.ven_id = c.ven_id   " + "				  JOIN detallev d   "
				+ "				 ON c.ven_id = d.ven_id       " + "				  JOIN articulo e   "
				+ "				   ON d.art_id = e.art_id   " + "				  WHERE b.dv_ven_id IS NULL   "
				+ "				  AND b.dv_art_id IS NULL   " + "				  AND a.folioNC IS NOT NULL   "
				+ "				  AND b.ncr_id=:ncr_id   " + "				 ORDER BY a.ncr_id";

		SQLQuery query = session.createSQLQuery(sql);

		SQLQuery queryDtl = session.createSQLQuery(sqlDtl);
		query.setResultTransformer(Transformers.aliasToBean(CreditNoteDTO.class));
		List<CreditNoteDTO> creditNotesFromSicar = query.list();
		List<CreditNoteDTO> creditNotesList = new ArrayList<CreditNoteDTO>();
		for (CreditNoteDTO ncHdr : creditNotesFromSicar) {
			int numLine = 1;
			BigDecimal recalculatedSubtotal = new BigDecimal(0); 
			double sumIva002_0 = 0;
			double sumIva002_16 = 0;
			double sumIeps003_0 = 0;
			double sumIeps003_06 = 0;
			double sumIeps003_07 = 0;
			double sumIeps003_09 = 0;

			// sum baseImp
			double sumBase002_16 = 0;
			double sumBase002_0 = 0;
			double sumBase003_06 = 0;
			double sumBase003_07 = 0;
			double sumBase003_09 = 0;

			queryDtl.setParameter("ncr_id", ncHdr.getNcr_id());
			queryDtl.setResultTransformer(Transformers.aliasToBean(CreditNoteDetailDTO.class));
			List<CreditNoteDetailDTO> detailCredNoteList = queryDtl.list();
			List<CreditNoteDetailDTO> detailList = new ArrayList<CreditNoteDetailDTO>();

			for (CreditNoteDetailDTO detailObj : detailCredNoteList) {

				detailObj.setFolioNc(ncHdr.getFolioNC());
				detailObj.setNumLinea(numLine);
				if (0.0 == detailObj.getTasaImpuestoTrasladado().doubleValue()
						&& "002".equals(detailObj.getTipoImpuestoTrasladado())) {
					sumIva002_0 = sumIva002_0 + detailObj.getImporteImpuestoTrasladado().doubleValue();
					sumBase002_0 = sumBase002_0 + detailObj.getBaseImpuestoTrasladado().doubleValue();
				}
				if (0.16 == detailObj.getTasaImpuestoTrasladado().doubleValue()
						&& "002".equals(detailObj.getTipoImpuestoTrasladado())) {
					sumIva002_16 = sumIva002_16 + detailObj.getImporteImpuestoTrasladado().doubleValue();
					sumBase002_16 = sumBase002_16 + detailObj.getBaseImpuestoTrasladado().doubleValue();
				}
				if (0.0 == detailObj.getTasaImpuestoTrasladado().doubleValue()
						&& "003".equals(detailObj.getTipoImpuestoTrasladado())) {
					sumIeps003_0 = sumIeps003_0 + detailObj.getImporteImpuestoTrasladado().doubleValue();
				}
				if (0.06 == detailObj.getTasaImpuestoTrasladado().doubleValue()
						&& "003".equals(detailObj.getTipoImpuestoTrasladado())) {
					sumIeps003_06 = sumIeps003_06 + detailObj.getImporteImpuestoTrasladado().doubleValue();
					sumBase003_06 = sumBase003_06 + detailObj.getBaseImpuestoTrasladado().doubleValue();
				}
				if (0.07 == detailObj.getTasaImpuestoTrasladado().doubleValue()
						&& "003".equals(detailObj.getTipoImpuestoTrasladado())) {
					sumIeps003_07 = sumIeps003_07 + detailObj.getImporteImpuestoTrasladado().doubleValue();
					sumBase003_07 = sumBase003_07 + detailObj.getBaseImpuestoTrasladado().doubleValue();
				}
				if (0.09 == detailObj.getTasaImpuestoTrasladado().doubleValue()
						&& "003".equals(detailObj.getTipoImpuestoTrasladado())) {
					sumIeps003_09 = sumIeps003_09 + detailObj.getImporteImpuestoTrasladado().doubleValue();
					sumBase003_09 = sumBase003_09 + detailObj.getBaseImpuestoTrasladado().doubleValue();
				}
				detailList.add(detailObj);
				numLine = numLine + 1;
				//sum Header
				recalculatedSubtotal = recalculatedSubtotal.add(detailObj.getBaseImpuestoTrasladado());
			}

			ncHdr.setSumIva002_0(sumIva002_0);
			ncHdr.setSumIva002_16(sumIva002_16);
			ncHdr.setSumIeps003_0(sumIeps003_0);
			ncHdr.setSumIeps003_06(sumIeps003_06);
			ncHdr.setSumIeps003_07(sumIeps003_07);
			ncHdr.setSumIeps003_09(sumIeps003_09);

			ncHdr.setSumBase002_0(sumBase002_0);
			ncHdr.setSumBase002_16(sumBase002_16);
			ncHdr.setSumBase003_06(sumBase003_06);
			ncHdr.setSumBase003_07(sumBase003_07);
			ncHdr.setSumBase003_09(sumBase003_09);
			
			//override header.Subtotal since there is wrong data from detallen
			ncHdr.setSubtotal(recalculatedSubtotal);
			ncHdr.setDetail(detailList);
			String sucursalId = String.format("%02d",sucursal.getId());
			ncHdr.setSucursal(sucursalId);
			creditNotesList.add(ncHdr);
		}

		return creditNotesList;
	}

}
