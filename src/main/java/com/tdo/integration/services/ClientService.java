package com.tdo.integration.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dto.integration.dto.CreditNoteDTO;
import com.dto.integration.dto.SicarCustomerDto;
import com.tdo.integration.client.model.CreditNoteRequestEntity;
import com.tdo.integration.client.model.CustomerAdditionalSpecs;
import com.tdo.integration.client.model.HistoricPriceCatalog;
import com.tdo.integration.client.model.SicarProductPrice;
import com.tdo.integration.client.model.Sucursal;
import com.tdo.integration.client.model.UDC;
import com.tdo.integration.dao.ClientDao;
import com.tdo.integration.dao.SicarDao;
import com.tdo.integration.util.AppConstants;

@Service("clientService")
public class ClientService {
	
	@Autowired
	ClientDao clientDao;
	
	@Autowired
	RESTService restService;
	
	@Autowired
	SicarDao sicarDao;
	
	private final static String TIPO_CAMBIO_KEY="TCAMBIO";
	private final static String INPROCESS ="INPROCESS";
	private final static String ERROR ="ERROR";
	private final static String NEW = "NEW";
	
	public void saveRecord(CustomerAdditionalSpecs o, String url, String sucursal) {
		o.setSucursal(sucursal);
		SicarCustomerDto dto = sicarDao.getCustomer(o.getCustomerId());
		if(dto != null) {
			o.setCustomerAddress(dto.getAddress());
			o.setCustomerEmail(dto.getEmail());
			o.setCustomerPhone(dto.getPhone());
		}
		boolean result = restService.sendRequestObject(url, o);
		if(result) {
			clientDao.saveRecord(o);
		}
	}
	
	public List<CustomerAdditionalSpecs> getAllCustomerSpecs() {
		return clientDao.getAllCustomerSpecs();
	}
	
	public void saveUDC(UDC o) {
		clientDao.saveUDC(o);
	}
	
	public void saveBranch(Sucursal s) {
		clientDao.saveBranch(s);
	}
	
	public Sucursal getBranch() {
		return clientDao.getBranch();
	}
	
	public List<Integer> getCustomerWithSpecsList(){
		return clientDao.getAllCustomerSpecsList();
	}
	
	public List<CustomerAdditionalSpecs> getCustomerSpecsList(int customerId){
		return clientDao.getCustomerSpecsList(customerId);
	}
	
	public List<UDC> getUdcBySystem(String system) {
		return clientDao.getUdcBySystem(system);
	}
	
	public List<String> getEstados() {
		return clientDao.getEstados();
	}
	
	public List<String> getMunicipios(String estado) {
		return clientDao.getMunicipios(estado);
	}
	
	public List<String> getLocalidades(String municipio) {
		return clientDao.getLocalidades(municipio);
	}
	
	public int getTotalRecords(int customerId){
		return clientDao.getTotalRecords(customerId);
	}

/**
	 * Service to send  historicPriceList to be persisted.
	 * @param historicPriceList
	 */
	public void saveHistoricPriceListService(List<HistoricPriceCatalog> historicPriceList){
		clientDao.saveHistoricPrice(historicPriceList);
	}
	
	/**
	 * Calculates the new price according the exchangeRate stored in UDC table.
	 * @param historicPriceList
	 * @return historicPriceList
	 */
	public List<HistoricPriceCatalog> updatePriceService(List<HistoricPriceCatalog> historicPriceList){
		
		List<UDC> exchangeRate = getUdcBySystem(TIPO_CAMBIO_KEY);
		List <HistoricPriceCatalog> processedList = new ArrayList<>();
		if(exchangeRate!=null) {
			for (HistoricPriceCatalog hpc : historicPriceList) {
				hpc.setCurrency(exchangeRate.get(0).getUdcKey());
				hpc.setExchangeRate(Double.valueOf(exchangeRate.get(0).getUdcValue()));
				processedList.add(hpc);
			}
		}
		return processedList;
	}
	
	/**
	 * service to save the generic retrieves in client db. NOT sicar db.
	 * @param sicarProductList
	 */
	public void saveSicarProductDto(List<SicarProductPrice> sicarProductList) {		
		clientDao.saveProductsDto(sicarProductList);
		
	}
	
	public List<SicarProductPrice> getSicarProductDtoListByItem(String clave){
		return clientDao.getSicarProductDtoListByItem(clave);
	}
	
	public List<HistoricPriceCatalog> getHistoricPriceListByItemId(String itemId){
		
		
		return clientDao.getHistoricPriceListByItem(itemId);
	}
	
	public List<SicarProductPrice> getSicarProductDtoList() {
		return clientDao.getSicarProductDtoList();
	}

	
	/**
	 * Invoked to get the status to show in the 
	 * @param o
	 * @param url
	 */
	public void  sendCreditNoteToIntegrator(CreditNoteDTO o, String url) {
		
		CreditNoteRequestEntity cnr = this.searchCreditNoteEntity(o);
		boolean result = restService.sendRequestObject(url, o);
//		CreditNoteRequestEntity cnr = clientDao.getCreditNoteRequestByFolio(o.getFolioNC().toString());
		cnr.setUpdatedBy("system");
		cnr.setLastUpdateTime(new Date());
		if(result) 
		{
			cnr.setStatus(INPROCESS);
			cnr.setCode("");
			cnr.setDesc("");
		}else {
			cnr.setStatus(ERROR);
			cnr.setCode(AppConstants.ERROR_300);
			cnr.setDesc(AppConstants.ERROR_300_DESC);	
		}
		clientDao.saveCreditNoteRequest(cnr);
		
	}
	
	public CreditNoteRequestEntity searchCreditNoteEntity(CreditNoteDTO cnDto) {
		
		CreditNoteRequestEntity cnr = this.getCreditNoteRequestByFolio(cnDto);
		if(cnr!=null) {
			return cnr;
		}else {
		   CreditNoteRequestEntity cnrNew = new CreditNoteRequestEntity();
		   cnrNew.setFolio(cnDto.getFolioNC().toString());
		   cnrNew.setDate(cnDto.getFecha());
		   cnrNew.setStatus(NEW);
		   cnrNew.setLastUpdateTime(new Date());
		   cnrNew.setUpdatedBy("system");
		   clientDao.saveCreditNoteRequest(cnrNew);
		   return cnrNew;
		}			
		
	} 
	
	public CreditNoteRequestEntity getCreditNoteRequestByFolio (CreditNoteDTO cnDto) {
		
		return clientDao.getCreditNoteRequestByFolio(cnDto.getFolioNC().toString());
		
	}
	
	public UDC getUdcByUdcKey(String udcKey,String system) {
		return clientDao.getUdcByUdcKeyAndSystem(udcKey,system);
	}
	
}
