package com.tdo.integration.services;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dto.integration.dto.CreditDTO;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service("creditLimitsService")
public class CreditLimitsService {
	
	@Autowired
	RESTService restService;
	
	static Logger logger = Logger.getLogger(CreditLimitsService.class);
	
	public List<CreditDTO> getCustomerCredit(String urlStr, String value) {
				
		List<CreditDTO> listCreditDTO = new ArrayList<CreditDTO>();
		
		try {
			logger.info("Se env�a petici�n al servicio: " + urlStr);
			String res = restService.sendRequestObjectString(urlStr, value);			
			JSONArray jsonArr = new JSONArray(res);

			logger.info("Respuesta: " + jsonArr.toString());
			for (int i = 0; i < jsonArr.length(); i++) {
		        JSONObject jsonObj = jsonArr.getJSONObject(i);
		        ObjectMapper mapper = new ObjectMapper();
		        CreditDTO c = mapper.readValue(jsonObj.toString(), CreditDTO.class);
		        listCreditDTO.add(c);
		    }
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Ocurri� un error al consumir el servicio " + urlStr, e);
		}
		
		return listCreditDTO;
	}
	
	@SuppressWarnings("serial")
	public List<String> updateCustomerCreditLimit(String urlStr, String numCustomer, String newCredit) {
		List<String> result = new ArrayList<String>(); 
		String response = "";
		
		try {			
			CreditDTO creditDTO = new CreditDTO();
			creditDTO.getCliente().setClienteNubeId(new BigInteger(numCustomer));
			creditDTO.getCliente().setClienteLimiteAbsoluto(new BigDecimal(newCredit));
			
			response = restService.sendRequestObjectString(urlStr, creditDTO);
			
            if(!"".equals(response)) {
            	ObjectMapper mapper = new ObjectMapper();
	            JsonNode jsonNode = mapper.readTree(response);
	            result.add(0, jsonNode.get("status").asText());
	            result.add(1, jsonNode.get("content").asText());
            }else {
            	result.add(0, "ERROR");
            	result.add(1, "Ha ocurrido un error inesperado. El resultado del ws-* es null");
            }
            
		} catch (Exception e) {
			e.printStackTrace();
			result.add(0, "ERROR");
			result.add(1, "Ha ocurrido un error inesperado al interpretar el resultado.");
		}
		
		return result;		
	}

	public static boolean isNumeric(String strNum) {
	    return strNum.matches("\\d+(\\.\\d+)?");
	}
	
}