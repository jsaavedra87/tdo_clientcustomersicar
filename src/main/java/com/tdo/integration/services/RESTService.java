package com.tdo.integration.services;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

@Service("restService")
public class RESTService {
	
	private HttpURLConnection conn;	
	static Logger logger = Logger.getLogger(CreditLimitsService.class);
	
	public boolean sendRequestObject(String urlStr, Object data) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			
			StringBuilder sb = new StringBuilder( mapper.writeValueAsString(data));
			String jsonInString = sb.toString();
			
			URL url = new URL(urlStr);
			conn = (HttpURLConnection) url.openConnection();
	  		conn.setDoInput(true);
	  		conn.setDoOutput(true);
	  		conn.setRequestMethod("POST");
	  		conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
	  		conn.setRequestProperty("Accept", "application/json");
	  		conn.setAllowUserInteraction(true);
			
			 OutputStream os = conn.getOutputStream();
	         OutputStreamWriter osw = new OutputStreamWriter(os);
	         byte[] outputBytes = jsonInString.getBytes("UTF-8");
	         os.write(outputBytes);
	         osw.flush();
	         osw.close();
	         os.close();
	         if (conn.getResponseCode() == 200) {
	        	 InputStream is = conn.getInputStream();
		            InputStreamReader isr = new InputStreamReader(is);
		            BufferedReader br = new BufferedReader(isr);
		            String line = null;
		            while ( (line = br.readLine()) != null)
		            {
		                System.out.println("line: " + line);
		            }
		            return true;
	 		}else {
	 			System.out.println("Error: " + conn.getResponseCode());
	 		}			
			conn.disconnect();

		} catch (MalformedURLException e) {
			e.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean sendRequestParameters(String urlStr, String udcValue, String udcKey) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			
			
			urlStr = urlStr + "?udcValue=" + udcValue +"&udcKey="+udcKey;
			URL url = new URL(urlStr);
			conn = (HttpURLConnection) url.openConnection();
	  		conn.setDoInput(true);
	  		conn.setDoOutput(true);
	  		conn.setRequestMethod("GET");
	  		conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
	  		conn.setRequestProperty("Accept", "application/json");
	  		conn.setAllowUserInteraction(true);
	  		String sb = readFullyAsString(conn.getInputStream(), "UTF-8");
	  		String jsonInString = sb.toString();

	         if (conn.getResponseCode() == 200) {
	        	 InputStream is = conn.getInputStream();
		            InputStreamReader isr = new InputStreamReader(is);
		            BufferedReader br = new BufferedReader(isr);
		            String line = null;
		            while ( (line = br.readLine()) != null)
		            {
		                System.out.println("line: " + line);
		            }
		            return true;
	 		}else {
	 			System.out.println("Error: " + conn.getResponseCode());
	 		}			
			conn.disconnect();

		} catch (MalformedURLException e) {
			e.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public String readFullyAsString(InputStream inputStream, String encoding){
		String result = "";
		try {
			result = readFully(inputStream).toString(encoding);
		}catch(Exception e) {
			e.printStackTrace();
			result = "";
		}
		return result;  
    }
	

    private ByteArrayOutputStream readFully(InputStream inputStream) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length = 0;
        while ((length = inputStream.read(buffer)) != -1) {
            baos.write(buffer, 0, length);
        }
        return baos;
    }
	
	public String sendRequestObjectString(String urlStr, Object data) {
		
        String resString = "";
		try {
			ObjectMapper mapper = new ObjectMapper();
			
			StringBuilder sb = new StringBuilder( mapper.writeValueAsString(data));
			String jsonInString = sb.toString();
		
			System.out.println(jsonInString);
			
			URL url = new URL(urlStr);
			conn = (HttpURLConnection) url.openConnection();
	  		conn.setDoInput(true);
	  		conn.setDoOutput(true);
	  		conn.setRequestMethod("POST");
	  		conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
	  		conn.setRequestProperty("Accept", "application/json");
	  		conn.setAllowUserInteraction(true);
			
			 OutputStream os = conn.getOutputStream();
	         OutputStreamWriter osw = new OutputStreamWriter(os);
	         byte[] outputBytes = jsonInString.getBytes("UTF-8");
	         os.write(outputBytes);
	         osw.flush();
	         osw.close();
	         os.close();
	         if (conn.getResponseCode() == 200) {
	        	 InputStream is = conn.getInputStream();

		            InputStreamReader isr = new InputStreamReader(is);
		            BufferedReader br = new BufferedReader(isr);
		            String line = null;
		            while ( (line = br.readLine()) != null)
		            {
		                resString = resString + line;
		            }
		            
		            if(!"".equals(resString)) {
//			            mapper = new ObjectMapper();
//			            JsonNode jsonNode = mapper.readTree(resString);
//			            System.out.println(jsonNode.get("content").asText());
		            }else {
		            	logger.error("Ha ocurrido un error inesperado. El resultado del ws-* es null");
		    			return "Ha ocurrido un error inesperado. El resultado del ws-* es null";
		            }
	 		}else {
	 			logger.error("Se obtiene una respuesta de error " + conn.getResponseCode());
				return "Error: " + conn.getResponseCode();				
	 		}			
			conn.disconnect();

		} catch (Exception e) {
			logger.error("Ocurri� un error al hacer la petici�n HTTP.", e);
			e.printStackTrace();
			return resString;
		}
		return resString;
	}
}
