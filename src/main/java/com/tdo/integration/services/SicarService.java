package com.tdo.integration.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dto.integration.dto.CreditNoteDTO;
import com.dto.integration.dto.SicarCustomerDto;
import com.dto.integration.dto.SicarItemDTO;
import com.tdo.integration.client.model.SicarProductPrice;
import com.tdo.integration.client.model.UDC;
import com.tdo.integration.dao.ClientDao;
import com.tdo.integration.dao.SicarDao;

@Service("sicarService")
public class SicarService {
	
	@Autowired
	SicarDao sicarDao;
	
	@Autowired
	RESTService restService;
	
	@Autowired 
	ClientDao clientDao;
	
	static Logger logger = Logger.getLogger(SicarService.class);
	private static final String PROCESSED="PROCESSED";
	
	public List<SicarCustomerDto> listCustomers(String name){
		return sicarDao.listCustomers(name);
	}

	/**
	 * Update Price according Last exchangeRate
	 */
	public void updateSicarItemPrice(List<SicarProductPrice> itemList) {

		sicarDao.updateSicarItemPrice(itemList);

	}

	/**
	 * Returns a list searching by ItemId
	 * @param itemId
	 * @return
	 */
	public List<SicarItemDTO> listItemsByItemId(String itemId) {

		return sicarDao.getSicarItemListBytItemId(itemId);
	}

	/**
	 * Used to get the total of records in articulo table
	 * 
	 * @return
	 */
	public List<Integer> getCountItems() {

		List<Integer> countList = new ArrayList<Integer>();
		countList.add(sicarDao.getCountItemList());
		return countList;
	}

	/**
     * Update tipoCambio into sicar.Moneda table.
	 * @throws IOException 
   */
    public boolean updateTipoCambioIntoMoneda(double tc) throws IOException {
    	
    Properties props = new Properties();
	props.load(SicarService.class.getResourceAsStream("/app.properties"));	
    
	List<UDC> udcList = clientDao.getUdcBySystem("TCAMBIO");
	UDC udc = udcList.get(0);
	udc.setUdcValue(String.valueOf(tc));
	
	logger.info("Se env�a a la tabla UDC el nuevo tipo de cambio: " + String.valueOf(tc));
	boolean result = restService.sendRequestParameters(props.getProperty("restSaveUdc"),String.valueOf(tc),udc.getUdcKey());
   	
	logger.info("Se obtiene resultado de " + props.getProperty("restSaveUdc") + ", " + String.valueOf(result));
   	if(result) {
   		clientDao.saveUDC(udc);  		
   		sicarDao.updateTipoCambioIntoMoneda(tc);
   		logger.info("Se actualiza la tabla de moneda en Sicar, nuevo tipo cambio: " +String.valueOf(tc));
   	}
   	
   	return result;
   }
    
    /**
     * get creditNotes List by RFC or Razon Social
     * @param name
     * @return
     */
	public List<CreditNoteDTO> listCreditNotes(String param){
		return sicarDao.listCreditNotes(param);
	}
	    
	public CreditNoteDTO getCreditNoteByFolio(String folio){
		List<CreditNoteDTO> list = sicarDao.getCreditNoteList(folio);
		 CreditNoteDTO cn = null;
		 if(list!=null && list.size()>0) {
			 cn = list.get(0);
		 }
		return cn;
	}


 /**
 * Update status to achieve item synchronization
 */
	public void updateSicarNubeAct(List<SicarProductPrice> sicarProductList) {
		sicarDao.updateSicarNubeAct(sicarProductList);		
	}
	
}
