package com.tdo.integration.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.Format;
import java.text.NumberFormat;
import java.util.List;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;

import org.springframework.beans.factory.annotation.Autowired;

import com.tdo.integration.services.CreditLimitsService;

public class CreditLimitsWindow {
	
	Properties props;
	
	@Autowired
	CreditLimitsService creditLimitsService;

	static JTextField customerNumber = new JTextField("");
	static JTextField customerName = new JTextField("");
	static JTextField customerRFC = new JTextField("");
	static JTextField customerCredit = new JTextField("");
	
	static Format nbr = NumberFormat.getNumberInstance();
	
	public JFrame getCreditLimitsPanel() {
		
		 JFrame frame = new JFrame("Actualizaci�n de L�mite de Cr�dito");
		 frame.setSize(700, 400);
		 frame.getContentPane().setBackground(Color.WHITE);
		 frame.setLocationRelativeTo(null);
		 
		 try 
         {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
         } catch (Exception e) {
            e.printStackTrace();
         }
		 
		 	Font fnt = new Font("SansSerif", Font.BOLD, 20);
		
		    //HEADER PANEL		 
		    
		    JPanel headerPanel = new JPanel();		    
		    headerPanel.setBackground(Color.WHITE);
		    
		    JLabel headerLabel = new JLabel("Informaci�n del Cliente");
		    headerLabel.setFont(fnt);
		    headerPanel.add(headerLabel);
			
		    // MAIN PANEL
			JPanel mainPanel = new JPanel();
			mainPanel.setBackground(Color.WHITE);
			mainPanel.setFont(fnt);
			mainPanel.setLayout(new GridBagLayout());
			
			GridBagConstraints constraints = new GridBagConstraints();
			constraints.insets = new Insets(5, 5, 5, 5);

		    customerNumber.setBorder(new LineBorder(Color.white,1)); 
		    customerNumber.setPreferredSize(new Dimension(500, 25));
		    customerNumber.setEditable(false);
		    customerNumber.setFont(fnt);
		    customerNumber.setHorizontalAlignment(JTextField.LEFT);
		    customerNumber.setBackground(Color.WHITE);
		    
			constraints.gridx = 0; 
			constraints.gridy = 0; 
			mainPanel.add(new JLabel("Nube Id: "), constraints);
			
			constraints.gridx = 1; 
			constraints.gridy = 0; 
			mainPanel.add(customerNumber, constraints);
		    
		    customerName.setBorder(new LineBorder(Color.white,1)); 
		    customerName.setPreferredSize(new Dimension(500, 25));
		    customerName.setEditable(false);
		    customerName.setFont(fnt);
		    customerName.setHorizontalAlignment(JTextField.LEFT);
		    customerName.setBackground(Color.WHITE);

			constraints.gridx = 0; 
			constraints.gridy = 1; 
			mainPanel.add(new JLabel("Nombre: "), constraints);
			
			constraints.gridx = 1; 
			constraints.gridy = 1; 
			mainPanel.add(customerName, constraints);
			
		    customerRFC.setBorder(new LineBorder(Color.white,1)); 
		    customerRFC.setPreferredSize(new Dimension(500, 25));
		    customerRFC.setEditable(false);
		    customerRFC.setFont(fnt);
		    customerRFC.setHorizontalAlignment(JTextField.LEFT);
		    customerRFC.setBackground(Color.WHITE);
		    
			constraints.gridx = 0;
			constraints.gridy = 2; 
		    mainPanel.add(new JLabel("RFC: "), constraints);
		    
			constraints.gridx = 1;
			constraints.gridy = 2;		    
		    mainPanel.add(customerRFC, constraints);
		    
		    customerCredit.setBorder(new LineBorder(Color.white,1)); 
		    customerCredit.setPreferredSize(new Dimension(500, 25));
		    customerCredit.setEditable(false);
		    customerCredit.setFont(fnt);
		    customerCredit.setHorizontalAlignment(JTextField.LEFT);
		    customerCredit.setBackground(Color.WHITE);
		    	
			constraints.gridx = 0; 
			constraints.gridy = 3; 
		    mainPanel.add(new JLabel("L�mite de Cr�dito: "), constraints);

			constraints.gridx = 1; 
			constraints.gridy = 3; 
		    mainPanel.add(customerCredit, constraints);

		    // BUTTONS PANEL
			JPanel buttonsPanel = new JPanel();
			buttonsPanel.setBackground(Color.WHITE);
			
			JButton saveButton = new JButton("Actualizar Cr�dito");
			saveButton.addActionListener(new ActionListener() {
		        public void actionPerformed(ActionEvent e) {
		        	updateRecord();
		        }
		    });

			buttonsPanel.add(saveButton);

		 frame.getContentPane().add(BorderLayout.NORTH, headerPanel);
		 frame.getContentPane().add(BorderLayout.CENTER, mainPanel);
		 frame.getContentPane().add(BorderLayout.SOUTH, buttonsPanel);
		 return frame;
		
	}

	public boolean updateRecord() {
		
		try {
			String value = JOptionPane.showInputDialog("Ingrese el nuevo l�mite de cr�dito:", customerCredit.getText().trim());
			
			if(value != null && !value.isEmpty()) {
				
				if(isNumeric(value)) {										
					List<String> res = creditLimitsService.updateCustomerCreditLimit(props.getProperty("restCustomerCreditLimit"), customerNumber.getText().trim(), value);
					
					if("OK".equals(res.get(0))) {
						customerCredit.setText(value.trim());
					}
					JOptionPane.showMessageDialog(null, res.get(1));
				} else {
					JOptionPane.showMessageDialog(null, "El valor del nuevo l�mite de cr�dito no es v�lido.");
				}
				
			} else {
				JOptionPane.showMessageDialog(null, "El valor del nuevo l�mite de cr�dito no es v�lido.");
			}

		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}

	public void setCustomerInfo(int custId, String custName, String custRFC, String custCredit) {
		customerNumber.setText(String.valueOf(custId).trim());
		customerName.setText(String.valueOf(custName).trim());
		customerRFC.setText(String.valueOf(custRFC).trim());
		customerCredit.setText(String.valueOf(custCredit).trim());
	}

	public void setProperties(Properties props) {
		this.props = props;
	}
	
	public static boolean isNumeric(String strNum) {
	    return strNum.matches("\\d+(\\.\\d+)?");
	}
}
