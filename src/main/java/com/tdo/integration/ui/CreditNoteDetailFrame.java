package com.tdo.integration.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Map;
import java.util.Properties;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;

import org.springframework.beans.factory.annotation.Autowired;

import com.dto.integration.dto.CreditNoteDTO;
import com.tdo.integration.client.AppActionListener;
import com.tdo.integration.services.ClientService;
import com.tdo.integration.services.SicarService;
import com.tdo.integration.util.AppConstants;

public class CreditNoteDetailFrame {

	Properties props;

	@Autowired
	SicarService sicarService;

	@Autowired
	ClientService clientService;

	@Autowired
	AppActionListener appAction;

	JTextField folio = new JTextField("");
	JTextField custName = new JTextField("");
	JTextField rfc = new JTextField("");
	JTextField qty = new JTextField("");
	JTextField ncDate = new JTextField("");
	JTextField totalNC = new JTextField("");
	JTextField status = new JTextField("");
	JTextField errorDesc = new JTextField("");
	JTextArea  errorCode = new JTextArea("");

	JFrame frame = null;
	private JMenuBar mb;
	private JMenu menu1;
	private JMenuItem mi1, mi2;

	public JFrame getCreditNotesPanel() {

		frame = new JFrame("Detalle de la nota de credito");
		frame.setSize(900, 450);
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setLocationRelativeTo(null);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				appAction.searchCreditNotes();
				frame.dispose();
			}
		});

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}

		// HEADER PANEL
		Font fnt = new Font("SansSerif", Font.BOLD, 20);
		Font fnt2 = new Font("SansSerif", Font.PLAIN, 20);
		Font fnt3 = new Font("SansSerif", Font.PLAIN, 18);


		// LEFT PANEL
		JPanel leftPanel = new JPanel();
		leftPanel.setBackground(Color.WHITE);
		leftPanel.setLayout(new GridBagLayout());

		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(5, 20, 5, 5);
		constraints.anchor = GridBagConstraints.WEST;

		constraints.gridx = 0;
		constraints.gridy = 0;
		JLabel folLbl = new JLabel("Folio:");
		folLbl.setFont(fnt);
		folLbl.setHorizontalAlignment(JTextField.LEFT);
		leftPanel.add(folLbl, constraints);

		folio.setBorder(new LineBorder(Color.white, 0));
		folio.setPreferredSize(new Dimension(30, 25));
		folio.setEditable(false);
		folio.setFont(fnt2);
		folio.setHorizontalAlignment(JTextField.LEFT);
		folio.setBackground(Color.WHITE);
		constraints.gridx = 1;
		constraints.gridy = 0;
		leftPanel.add(folio, constraints);

		constraints.gridx = 0;
		constraints.gridy = 1;
		JLabel custLbl = new JLabel("Cliente:");
		custLbl.setFont(fnt);
		custLbl.setHorizontalAlignment(JTextField.LEFT);
		leftPanel.add(custLbl, constraints);

		custName.setBorder(new LineBorder(Color.white, 1));
		custName.setEditable(false);
		custName.setFont(fnt2);
		custName.setHorizontalAlignment(JTextField.LEFT);
		custName.setBackground(Color.WHITE);
		custName.setPreferredSize(new Dimension(700, 29));
		constraints.gridx = 1;
		constraints.gridy = 1;
		leftPanel.add(custName, constraints);

		constraints.gridx = 0;
		constraints.gridy = 2;
		JLabel rfcLbl = new JLabel("RFC:");
		rfcLbl.setFont(fnt);
		rfcLbl.setHorizontalAlignment(JTextField.LEFT);
		leftPanel.add(rfcLbl, constraints);

		rfc.setPreferredSize(new Dimension(200, 29));
		rfc.setBorder(new LineBorder(Color.white, 1));
		rfc.setEditable(false);
		rfc.setFont(fnt2);
		rfc.setHorizontalAlignment(JTextField.LEFT);
		rfc.setBackground(Color.WHITE);
		constraints.gridx = 1;
		constraints.gridy = 2;
		leftPanel.add(rfc, constraints);

		constraints.gridx = 0;
		constraints.gridy = 3;
		JLabel ncDateLbl = new JLabel("Fecha Documento:");
		ncDateLbl.setFont(fnt);
		ncDateLbl.setHorizontalAlignment(JTextField.LEFT);
		leftPanel.add(ncDateLbl, constraints);

		ncDate.setPreferredSize(new Dimension(250, 29));
		ncDate.setBorder(new LineBorder(Color.white, 1));
		ncDate.setEditable(false);
		ncDate.setFont(fnt2);
		ncDate.setHorizontalAlignment(JTextField.LEFT);
		ncDate.setBackground(Color.WHITE);
		constraints.gridx = 1;
		constraints.gridy = 3;
		leftPanel.add(ncDate, constraints);

		constraints.gridx = 0;
		constraints.gridy = 4;
		JLabel totalNCLbl = new JLabel("Importe Total $:");
		totalNCLbl.setFont(fnt);
		totalNCLbl.setHorizontalAlignment(JTextField.LEFT);
		leftPanel.add(totalNCLbl, constraints);

		totalNC.setPreferredSize(new Dimension(150, 29));
		totalNC.setBorder(new LineBorder(Color.white, 1));
		totalNC.setEditable(false);
		totalNC.setFont(fnt2);
		totalNC.setHorizontalAlignment(JTextField.LEFT);
		totalNC.setBackground(Color.WHITE);
		constraints.gridx = 1;
		constraints.gridy = 4;
		leftPanel.add(totalNC, constraints);

		constraints.gridx = 0;
		constraints.gridy = 5;
		JLabel stsLbl = new JLabel("Status:");
		stsLbl.setFont(fnt);
		stsLbl.setHorizontalAlignment(JTextField.LEFT);
		leftPanel.add(stsLbl, constraints);

		status.setPreferredSize(new Dimension(120, 29));
		status.setBorder(new LineBorder(Color.white, 1));
		status.setEditable(false);
		status.setFont(fnt2);
		status.setHorizontalAlignment(JTextField.LEFT);
		status.setBackground(Color.WHITE);
		constraints.gridx = 1;
		constraints.gridy = 5;
		leftPanel.add(status, constraints);

		if("Error".equals(status.getText())){
			constraints.gridx = 0;
			constraints.gridy = 6;
			JLabel errLbl = new JLabel("Descripci�n Error:");
			errLbl.setFont(fnt);
			errLbl.setHorizontalAlignment(JTextField.LEFT);
			leftPanel.add(errLbl, constraints);
		}
		
		errorCode.setPreferredSize(new Dimension(650, 70));
		errorCode.setBorder(new LineBorder(Color.white, 1));
		errorCode.setEditable(false);
		errorCode.setFont(fnt3);
//		errorCode.setHorizontalAlignment(JTextField.LEFT);
		errorCode.setLineWrap(true);
		errorCode.setBackground(Color.white);
		constraints.gridx = 1;
		constraints.gridy = 6;
		leftPanel.add(errorCode, constraints);

		JButton sendButton = new JButton("Timbrar");
		sendButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CreditNoteDTO cnrdto = sicarService.getCreditNoteByFolio(folio.getText());
				sendCreditNotes(cnrdto);

			}
		});

		JButton validateButton = new JButton("Validar Datos");
		validateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				validateRecord();
			}
		});

		JPanel buttonsPanel = new JPanel();
		buttonsPanel.setBackground(Color.WHITE);
		Map<String, String> allowedStatuses = appAction.getAllStatuses();

		if (allowedStatuses != null) {
			String key = allowedStatuses.get(status.getText());
			if (AppConstants.NC_NEW_STATUS.equals(key) || AppConstants.NC_ERROR_STATUS.equals(key)) {
				buttonsPanel.add(sendButton);
			}
		}

		constraints.gridx = 1;
		constraints.gridy = 10;
		leftPanel.add(buttonsPanel, constraints);

		frame.getContentPane().add(BorderLayout.WEST, leftPanel);
		return frame;

	}

	public void setProperties(Properties props) {
		this.props = props;
	}

	/**
	 * send the creditNoteToBeProcessed in integrator.
	 * 
	 * @param o
	 */
	public void sendCreditNotes(CreditNoteDTO o) {

		Object[] options = { "No", "Si" };
		int n = JOptionPane.showOptionDialog(frame, "�Desea enviar a timbrar la NC" + o.getFolioNC().toString() +" ?",
				null, JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options,options[1]);

		if (n == 1) {
			clientService.sendCreditNoteToIntegrator(o, props.getProperty("restCreditNotes"));
			appAction.searchCreditNotes();
			frame.dispose();
		}
	}

	public void validateRecord() {

	}

	public void loadDetailData(String folioNc, String cliente, String rfcC, String total, String fechadoc,
			String statusVal,String errorCodeVal, String errorDescVal) {

		folio.setText(folioNc);
		custName.setText(cliente);
		rfc.setText(rfcC);
		ncDate.setText(fechadoc);
		totalNC.setText(total);
		if("Error".equals(statusVal)){
			errorCode.setText(errorDescVal);
		}else {
			errorCode.setText("");
		}
		status.setText(statusVal);

	}

}
