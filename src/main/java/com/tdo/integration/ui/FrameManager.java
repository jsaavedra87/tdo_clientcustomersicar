package com.tdo.integration.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.RowSorterEvent;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.tdo.integration.client.AppActionListener;
import com.tdo.integration.client.MainClient;
import com.tdo.integration.client.SpecsWindow;
import com.tdo.integration.client.model.SicarProductPrice;

/**
 * 
 * @author arell Frame Manager class, contents definition of frames
 */
@SuppressWarnings("unused")
public class FrameManager {
	
	private Properties props;

	private JFrame frame;
	private static JMenuBar mb;
	private static JMenu menu1;
	private static JMenuItem mi1;
	private static JMenuBar mainMenuBar;
	private static JMenu mainMenu;
	private static JMenuItem exchangeRateItem;
	private static JMenuItem cultivosItem;
	private static JMenuItem creditLimitItem;
	private static JMenuItem creditNotesItem;
	
	private JFrame mainFrame;
	private final String MAIN_HEADER = "Tomate de Oro";
	private final String GO_BACK = "Menu Principal";
	private static JTextField tipoCambio;
	private JFrame productPrice;
	
	static Logger logger = Logger.getLogger(FrameManager.class);
	
	/**
	 * main menu
	 * 
	 * @param bl
	 * @param sw
	 * @return
	 */
	public JFrame mainFrame(AppActionListener bl) {

		this.mainFrame = new JFrame(MAIN_HEADER);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setSize(1200, 600);
		mainFrame.setLocationRelativeTo(null);
		JLabel l1;
		JLabel menuLabel = new JLabel();
		menuLabel.setLayout(new FlowLayout());
		Font font = new Font(Font.DIALOG, Font.BOLD, 26);
		l1 = new JLabel("MENU PRINCIPAL");
		l1.setFont(font);
		ImageIcon logo = new ImageIcon("C:\\Tomate.jpg");
		JLabel logoLbl = new JLabel(logo);
		JPanel centerPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		menuLabel.add(l1);
		mainFrame.add(centerPanel);
		mainFrame.add(menuLabel);

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Barra de Menu, ventana principal
		mainMenuBar = new JMenuBar();
		mainFrame.setJMenuBar(mainMenuBar);
		mainMenu = new JMenu("Opciones de sistema");
		mainMenuBar.add(mainMenu);
		// tipo de cambio
		exchangeRateItem = new JMenuItem("Administraci�n de Precios");
		exchangeRateItem.setActionCommand("OPEN_ITEM_PRICE");
		exchangeRateItem.addActionListener(bl);
		mainMenu.add(exchangeRateItem);
		// tipos de cultivo
		cultivosItem = new JMenuItem("Tipo de Cultivo");
		cultivosItem.setActionCommand("OPEN_CULTIVO_ITEM");
		cultivosItem.addActionListener(bl);
		mainMenu.add(cultivosItem);
		// limites cr�dito
		creditLimitItem = new JMenuItem("L�mites de Cr�dito");
		creditLimitItem.setActionCommand("OPEN_ITEM_CREDIT");
		creditLimitItem.addActionListener(bl);
		mainMenu.add(creditLimitItem);
		//Notas de Credito
		creditNotesItem = new JMenuItem("Notas de Cr�dito");
		creditNotesItem.setActionCommand("OPEN_ITEM_NC");
		creditNotesItem.addActionListener(bl);
		mainMenu.add(creditNotesItem);

		JPanel exitPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		exitPanel.setBackground(Color.WHITE);
		JButton exitButton = this.getExitButton();
		exitPanel.add(exitButton);
		// Adding Components to the frame.
		mainFrame.getContentPane().add(BorderLayout.SOUTH, exitPanel);

		return mainFrame;
	}

	/**
	 * Returns Tipo de Cultivo Frame
	 * 
	 * @param AppActionListener bl
	 * @param SpecsWindow       sw
	 * @return
	 */
	public JFrame cultivoManagerFrame(AppActionListener bl, SpecsWindow sw) {
		// Creating the Frame
		JFrame frame = new JFrame("Tomate de Oro - Informaci�n adicional de clientes");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setSize(1200, 600);
		frame.setResizable(false);
		frame.setUndecorated(true);
		frame.setLocationRelativeTo(null);

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}

		mb = new JMenuBar();
		frame.setJMenuBar(mb);
		menu1 = new JMenu("Opciones locales");
		mb.add(menu1);
		mi1 = new JMenuItem("Registrar sucursal");
		mi1.setActionCommand("SAVE_BRANCH");
		mi1.addActionListener(bl);
		menu1.add(mi1);

		JLabel searchLabel = new JLabel("Buscar cliente: ");
		JTextField searchText = new JTextField("");
		bl.setCustomerSearchField(searchText);
		searchText.setPreferredSize(new Dimension(800, 29));
		JButton searchCustomer = new JButton("Buscar Cliente");
		searchCustomer.setActionCommand("SEARCH_CUST");
		searchCustomer.addActionListener(bl);

		JButton downloadRecords = new JButton("Exportar datos");
		downloadRecords.setActionCommand("DOWNLOAD_RECORDS");
		downloadRecords.addActionListener(bl);
					
		JPanel searchPanel = new JPanel();
		searchPanel.setLayout(new GridBagLayout());

		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(12, 12, 12, 2);
		constraints.gridx = 0; // El �rea de texto empieza en la columna cero.
		constraints.gridy = 0; // El �rea de texto empieza en la fila cero
		searchPanel.add(searchLabel, constraints);

		constraints.gridx = 1; // El �rea de texto empieza en la columna cero.
		constraints.gridy = 0; // El �rea de texto empieza en la fila cero
		searchPanel.add(searchText, constraints);

		constraints.gridx = 2; // El �rea de texto empieza en la columna cero.
		constraints.gridy = 0; // El �rea de texto empieza en la fila cero
		searchPanel.add(searchCustomer, constraints);

		constraints.gridx = 3; // El �rea de texto empieza en la columna cero.
		constraints.gridy = 0; // El �rea de texto empieza en la fila cero
		searchPanel.add(downloadRecords, constraints);

		String[] columnNames = { "Num Cliente", "Nombre", "email", "Direcci�n", "Tel�fono", "Registro" };
		DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		JTable customerTable = new JTable(model);
		customerTable.setFocusable(false);
		customerTable.setRowSelectionAllowed(true);
		customerTable.setBounds(30, 40, 200, 300);
		customerTable.setRowHeight(25);
		TableColumnModel columnModel = customerTable.getColumnModel();
		columnModel.getColumn(0).setPreferredWidth(50);
		columnModel.getColumn(1).setPreferredWidth(200);
		columnModel.getColumn(2).setPreferredWidth(200);
		columnModel.getColumn(3).setPreferredWidth(350);
		columnModel.getColumn(4).setPreferredWidth(80);
		columnModel.getColumn(5).setPreferredWidth(50);
		JScrollPane sp = new JScrollPane(customerTable);
		bl.setCustomerTable(customerTable);
		
		Properties mainProps = this.props;

		customerTable.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				int row = customerTable.rowAtPoint(evt.getPoint());
				int col = customerTable.columnAtPoint(evt.getPoint());
				if (row >= 0 && col >= 0) {
					int cid = Integer.valueOf(customerTable.getValueAt(customerTable.getSelectedRow(), 0).toString());
					String cn = customerTable.getValueAt(customerTable.getSelectedRow(), 1).toString();
					sw.setHeader(cid, cn);
					sw.setProperties(mainProps);
					sw.getSpecsPanel().setVisible(true);
				}
			}
		});

		JPanel goBackPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		goBackPanel.setBackground(Color.WHITE);
		JButton goBackButton = new JButton(GO_BACK);
		goBackButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
			}
		});
		goBackPanel.add(goBackButton);

		JButton exitButton = this.getExitButton();
		goBackPanel.add(exitButton);

		// Adding Components to the frame.
		frame.getContentPane().add(BorderLayout.SOUTH, goBackPanel);
		frame.getContentPane().add(BorderLayout.NORTH, searchPanel);
		frame.getContentPane().add(BorderLayout.CENTER, sp);
		frame.getRootPane().setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.GRAY));
		frame.setVisible(true);
		return frame;

	}

	public JFrame itemPriceFrame(AppActionListener bl) {

		// Creating the Frame
		JFrame itemPriceFrame = new JFrame("Tomate de Oro - Administraci�n de Precios");
		itemPriceFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		itemPriceFrame.setSize(1100, 550);
		itemPriceFrame.setResizable(false);
		itemPriceFrame.setUndecorated(true);
		itemPriceFrame.setLocationRelativeTo(null);

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}

		JLabel searchLabel = new JLabel("Buscar Articulo: ");
		JLabel tc = new JLabel("T.C. Actual: ");
		//tipoCambio = bl.setCurrentExchangeRate();
		JTextField searchText = new JTextField("");
		bl.setItemField(searchText);
		searchText.setPreferredSize(new Dimension(450, 29));
		JButton searchItem = new JButton("Buscar");
		searchItem.setActionCommand("SEARCH_ITEM");
		searchItem.addActionListener(bl);
		JPanel searchPanel = new JPanel();
		searchPanel.setLayout(new GridBagLayout());
		itemPriceFrame.add(searchPanel);

		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(20, 8, 12, 2);
		constraints.gridx = 0; // El �rea de texto empieza en la columna cero.
		constraints.gridy = 0; // El �rea de texto empieza en la fila cero
		searchPanel.add(searchLabel, constraints);

		constraints.gridx = 1; // El �rea de texto empieza en la columna cero.
		constraints.gridy = 0; // El �rea de texto empieza en la fila cero
		searchPanel.add(searchText, constraints);

		constraints.gridx = 2; // El �rea de texto empieza en la columna cero.
		constraints.gridy = 0; // El �rea de texto empieza en la fila cero
		searchPanel.add(searchItem, constraints);
		constraints.gridx = 3; // El �rea de texto empieza en la columna cero.
		constraints.gridy = 0; // El �rea de texto empieza en la fila cero
		//searchPanel.add(tc, constraints);
		constraints.gridx = 4; // El �rea de texto empieza en la columna cero.
		constraints.gridy = 0; // El �rea de texto empieza en la fila cero
		//searchPanel.add(tipoCambio, constraints);

		String[] columnNames = {"itemIdentifier","description","exchangeRate","currency","purchasePriceAvrg","updatedDate","id"};
		DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		JTable itemTable = new JTable(model);
		itemTable.setFocusable(false);
		itemTable.setRowSelectionAllowed(true);
		itemTable.setBounds(30, 40, 200, 300);
		itemTable.setRowHeight(25);
		JTableHeader hdr = itemTable.getTableHeader();
		hdr.setFont(hdr.getFont().deriveFont(Font.BOLD)); 
		hdr.setBackground(Color.GRAY);
		hdr.setOpaque(true);
		TableColumnModel columnModel = itemTable.getColumnModel();
		columnModel.getColumn(0).setPreferredWidth(10);
		columnModel.getColumn(1).setPreferredWidth(10);
		columnModel.getColumn(2).setPreferredWidth(10);
		columnModel.getColumn(3).setPreferredWidth(10);
		columnModel.getColumn(4).setPreferredWidth(20);
		columnModel.getColumn(5).setPreferredWidth(5);
		columnModel.getColumn(6).setPreferredWidth(20);
		//headers 
		columnModel.getColumn(0).setHeaderValue("Clave");
		columnModel.getColumn(1).setHeaderValue("Descripci�n");
		columnModel.getColumn(2).setHeaderValue("T.C.");
		columnModel.getColumn(3).setHeaderValue("Moneda");
		columnModel.getColumn(4).setHeaderValue("Costo Promedio");
		columnModel.getColumn(5).setHeaderValue("Fecha Modificaci�n");
		columnModel.getColumn(6).setHeaderValue("Id Registro");
		
		JScrollPane sp = new JScrollPane(itemTable);
		bl.setItemTable(itemTable);

		JPanel goBackPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		goBackPanel.setBackground(Color.WHITE);
		JButton goBackButton = new JButton(GO_BACK);
		goBackButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				itemPriceFrame.setVisible(false);
			}
		});
		goBackPanel.add(goBackButton);

		goBackPanel.add(this.getExitButton());

		// Adding Components to the frame.
		itemPriceFrame.getContentPane().add(BorderLayout.SOUTH, goBackPanel);
		itemPriceFrame.getContentPane().add(BorderLayout.NORTH, searchPanel);
		itemPriceFrame.getContentPane().add(BorderLayout.CENTER, sp);
		itemPriceFrame.getRootPane().setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.GRAY));

		itemTable.addMouseListener(new java.awt.event.MouseAdapter() {
			@SuppressWarnings("resource")
			@Override
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				
					int row = itemTable.rowAtPoint(evt.getPoint());
				    int col = itemTable.columnAtPoint(evt.getPoint());
					if (row >= 0 && col >= 0) {
						ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Module.xml");
						String itemId = itemTable.getValueAt(itemTable.getSelectedRow(), 0).toString();
						//String itemDescription = itemTable.getValueAt(itemTable.getSelectedRow(), 1).toString();
						String itemDescription = "";
						ProductPriceDetails sw = (ProductPriceDetails)context.getBean("productPriceDetails");
						sw.setParentTable(itemTable);
						productPrice = sw.getProductPricePanel();
						SicarProductPrice obj = new SicarProductPrice();
						//setting values from this table to detail frame
						obj.setId(Integer.valueOf(itemTable.getValueAt(itemTable.getSelectedRow(),6).toString()));
						obj.setItemIdentifier(itemId);
						obj.setDescription(itemDescription);
						obj.setPurchasePriceAvrg(Double.valueOf(itemTable.getValueAt(itemTable.getSelectedRow(), 4).toString()));
						obj.setExchangeRate(Double.valueOf(itemTable.getValueAt(itemTable.getSelectedRow(), 2).toString()));
						sw.setHeader(obj);
						
						if(!productPrice.isVisible()) {
							productPrice.setVisible(true);
						}else {
							JOptionPane.showMessageDialog(null,"Ya existe otro detalle de precios abierto");
						}
					}
			  }
		});

		return itemPriceFrame;

	}

	/**
	 * Returns Credit Limits Frame
	 * 
	 * @param AppActionListener bl
	 * @param SpecsWindow       sw
	 * @return
	 */
	public JFrame creditLimitManagerFrame(AppActionListener bl, CreditLimitsWindow clw) {
		
		// Creating the Frame
		JFrame frame = new JFrame("Tomate de Oro - L�mites de cr�dito");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setSize(1200, 600);
		frame.setResizable(false);
		frame.setUndecorated(true);
		frame.setLocationRelativeTo(null);


		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}

		mb = new JMenuBar();
		frame.setJMenuBar(mb);
		menu1 = new JMenu("Opciones locales");
		mb.add(menu1);
		mi1 = new JMenuItem("Registrar sucursal");
		mi1.setActionCommand("SAVE_BRANCH");
		mi1.addActionListener(bl);
		menu1.add(mi1);
				
		JLabel searchLabel = new JLabel("Buscar cliente: ");
		JTextField searchText = new JTextField("");
		bl.setCustomerSearchField(searchText);
		searchText.setPreferredSize(new Dimension(800, 29));
		JButton searchCustomer = new JButton("Buscar Cliente");
		searchCustomer.setActionCommand("SEARCH_CUST_CREDIT");
		searchCustomer.addActionListener(bl);
			
		JPanel searchPanel = new JPanel();
		searchPanel.setLayout(new GridBagLayout());

		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(12, 12, 12, 2);
		constraints.gridx = 0; // El �rea de texto empieza en la columna cero.
		constraints.gridy = 0; // El �rea de texto empieza en la fila cero
		searchPanel.add(searchLabel, constraints);

		constraints.gridx = 1; // El �rea de texto empieza en la columna cero.
		constraints.gridy = 0; // El �rea de texto empieza en la fila cero
		searchPanel.add(searchText, constraints);

		constraints.gridx = 2; // El �rea de texto empieza en la columna cero.
		constraints.gridy = 0; // El �rea de texto empieza en la fila cero
		searchPanel.add(searchCustomer, constraints);

		String[] columnNames = { "Num Cliente Nube", "Nombre", "RFC", "CURP", "L�mite Cr�dito" };
		
		DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		JTable customerTable = new JTable(model);
		customerTable.setFocusable(false);
		customerTable.setRowSelectionAllowed(true);
		customerTable.setBounds(30, 40, 200, 300);
		customerTable.setRowHeight(25);
		TableColumnModel columnModel = customerTable.getColumnModel();
		columnModel.getColumn(0).setPreferredWidth(50);
		columnModel.getColumn(1).setPreferredWidth(200);
		columnModel.getColumn(2).setPreferredWidth(100);
		columnModel.getColumn(3).setPreferredWidth(100);
		columnModel.getColumn(4).setPreferredWidth(100);
		JScrollPane sp = new JScrollPane(customerTable);
		bl.setCustomerTable(customerTable);
		
		Properties mainProps = this.props;

		customerTable.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				int row = customerTable.rowAtPoint(evt.getPoint());
				int col = customerTable.columnAtPoint(evt.getPoint());
				if (row >= 0 && col >= 0) {
					int cid = Integer.valueOf(customerTable.getValueAt(customerTable.getSelectedRow(), 0).toString());
					String cn = customerTable.getValueAt(customerTable.getSelectedRow(), 1).toString();
					String crfc = customerTable.getValueAt(customerTable.getSelectedRow(), 2).toString();
					String cc = customerTable.getValueAt(customerTable.getSelectedRow(), 4).toString();
					clw.setProperties(mainProps);
					clw.setCustomerInfo(cid, cn, crfc, cc);
					clw.getCreditLimitsPanel().setVisible(true);
				}
			}
		});

		JPanel goBackPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		goBackPanel.setBackground(Color.WHITE);
		JButton goBackButton = new JButton(GO_BACK);
		goBackButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
			}
		});
		goBackPanel.add(goBackButton);

		JButton exitButton = this.getExitButton();
		goBackPanel.add(exitButton);

		// Adding Components to the frame.
		frame.getContentPane().add(BorderLayout.SOUTH, goBackPanel);
		frame.getContentPane().add(BorderLayout.NORTH, searchPanel);
		frame.getContentPane().add(BorderLayout.CENTER, sp);
		frame.getRootPane().setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.GRAY));
		frame.setVisible(true);
		return frame;

	}
	
	
	
	public JFrame creditNoteMainFrame(AppActionListener bl, CreditNoteDetailFrame ncDetail) {
		// Creating the Frame
		JFrame frame = new JFrame("Tomate de Oro - Notas de Credito");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setSize(1200, 600);
		frame.setResizable(false);
		frame.setUndecorated(true);
		frame.setLocationRelativeTo(null);

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}

		JLabel searchLabel = new JLabel("Buscar Notas de Credito: ");
		JTextField searchText = new JTextField("");
		bl.setCustomerSearchField(searchText);
		searchText.setPreferredSize(new Dimension(800, 29));
		searchText.setToolTipText("Busqueda por RFC o Raz�n social");
		JButton searchNCButton = new JButton("Buscar");
		searchNCButton.setActionCommand("SEARCH_NC");
		searchNCButton.addActionListener(bl);

		JPanel searchPanel = new JPanel();
		searchPanel.setLayout(new GridBagLayout());

		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(12, 12, 12, 2);
		constraints.gridx = 0; // El �rea de texto empieza en la columna cero.
		constraints.gridy = 0; // El �rea de texto empieza en la fila cero
		searchPanel.add(searchLabel, constraints);

		constraints.gridx = 1; // El �rea de texto empieza en la columna cero.
		constraints.gridy = 0; // El �rea de texto empieza en la fila cero
		searchPanel.add(searchText, constraints);

		constraints.gridx = 2; // El �rea de texto empieza en la columna cero.
		constraints.gridy = 0; // El �rea de texto empieza en la fila cero
		searchPanel.add(searchNCButton, constraints);

		String[] columnNames = { "Folio", "Cliente", "RFC", "Total $", "Fecha Documento", "Status","C�digo Respuesta","Descripci�n Respuesta" };
		DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		JTable creditNotesTable = new JTable(model);
		creditNotesTable.setFocusable(false);
		creditNotesTable.setRowSelectionAllowed(true);
		creditNotesTable.setBounds(30, 40, 200, 300);
		creditNotesTable.setRowHeight(25);
		TableColumnModel columnModel = creditNotesTable.getColumnModel();
		columnModel.getColumn(0).setPreferredWidth(50);
		columnModel.getColumn(1).setPreferredWidth(300);
		columnModel.getColumn(2).setPreferredWidth(150);
		columnModel.getColumn(3).setPreferredWidth(150);
		columnModel.getColumn(4).setPreferredWidth(130);
		columnModel.getColumn(5).setPreferredWidth(60);
		columnModel.getColumn(6).setPreferredWidth(80);
		columnModel.getColumn(7).setPreferredWidth(100);
		JScrollPane sp = new JScrollPane(creditNotesTable);
		bl.setCreditNotesTables(creditNotesTable);
		
		Properties mainProps = this.props;

		creditNotesTable.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				int row = creditNotesTable.rowAtPoint(evt.getPoint());
				int col = creditNotesTable.columnAtPoint(evt.getPoint());
				if (row >= 0 && col >= 0) {
					int cid = Integer.valueOf(creditNotesTable.getValueAt(creditNotesTable.getSelectedRow(), 0).toString());
					String cn = creditNotesTable.getValueAt(creditNotesTable.getSelectedRow(), 1).toString();
					Map <String,String> map = new HashMap<String,String>();
					//method to populate the detail form
					String folio = creditNotesTable.getValueAt(creditNotesTable.getSelectedRow(), 0).toString();
					String cliente = creditNotesTable.getValueAt(creditNotesTable.getSelectedRow(), 1).toString();
					String rfc = creditNotesTable.getValueAt(creditNotesTable.getSelectedRow(), 2).toString();
					String total = creditNotesTable.getValueAt(creditNotesTable.getSelectedRow(), 3).toString();
					String fechadoc = creditNotesTable.getValueAt(creditNotesTable.getSelectedRow(), 4).toString();
					String status= creditNotesTable.getValueAt(creditNotesTable.getSelectedRow(), 5).toString();
					String errorCode = creditNotesTable.getValueAt(creditNotesTable.getSelectedRow(), 6).toString();
					String errorDesc = creditNotesTable.getValueAt(creditNotesTable.getSelectedRow(), 7).toString();
					
					ncDetail.loadDetailData(folio,cliente,rfc,total,fechadoc,status,errorCode,errorDesc);
					ncDetail.setProperties(mainProps);
					ncDetail.getCreditNotesPanel().setVisible(true);
				}
			}
		});

		JPanel goBackPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		goBackPanel.setBackground(Color.WHITE);
		JButton goBackButton = new JButton(GO_BACK);
		goBackButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
			}
		});
		goBackPanel.add(goBackButton);

		JButton exitButton = this.getExitButton();
		goBackPanel.add(exitButton);

		// Adding Components to the frame.
		frame.getContentPane().add(BorderLayout.SOUTH, goBackPanel);
		frame.getContentPane().add(BorderLayout.NORTH, searchPanel);
		frame.getContentPane().add(BorderLayout.CENTER, sp);
		frame.getRootPane().setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.GRAY));
		frame.setVisible(true);
		return frame;
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(300, 300, 300, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	/**
	 * Returns a Exit button
	 */
	public JButton getExitButton() {
		JButton exitButton = new JButton("Salir");
		exitButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int input = JOptionPane.showConfirmDialog(null, "Desea cerrar la aplicaci�n?");
				if (input == 0) {
					logger.info(":::::::::::::::::::::::::: SE CIERRA LA APLICACI�N ::::::::::::::::::::::::::");
					System.exit(WindowConstants.EXIT_ON_CLOSE);
				}
			}
		});

		return exitButton;

	}
	
	public void setProperties(Properties props) {
		this.props = props;
	}

}
