package com.tdo.integration.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagLayout;
import java.util.Properties;

import javax.swing.GroupLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import org.springframework.beans.factory.annotation.Autowired;

import com.tdo.integration.services.ClientService;
import com.tdo.integration.services.SicarService;

public class HistoricPriceWindow {

	Properties props;

	@Autowired
	SicarService sicarService;

	@Autowired
	ClientService clientService;

	public JFrame getItemPriceForm() {

		JFrame frame = new JFrame("Historico de Precios por articulo");
		frame.setSize(700, 300);
		frame.setLocationRelativeTo(null);
		String[] columnNames = { "Id articulo", "currency", "fecha", "base price","tipo de cambio","precio equivalente" };
		DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		JTable itemTable = new JTable(model);
		TableColumnModel columnModel = itemTable.getColumnModel();
		columnModel.getColumn(0).setPreferredWidth(20);
		columnModel.getColumn(1).setPreferredWidth(30);
		columnModel.getColumn(2).setPreferredWidth(50);
		columnModel.getColumn(3).setPreferredWidth(30);
		columnModel.getColumn(4).setPreferredWidth(30);
		columnModel.getColumn(5).setPreferredWidth(30);
		JScrollPane sp = new JScrollPane(itemTable);
		frame.getContentPane().add(BorderLayout.CENTER, sp);

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return frame;
	}

}
