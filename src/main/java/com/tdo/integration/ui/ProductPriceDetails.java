package com.tdo.integration.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.tdo.integration.client.model.HistoricPriceCatalog;
import com.tdo.integration.client.model.SicarProductPrice;
import com.tdo.integration.services.ClientService;
import com.tdo.integration.services.SicarService;

public class ProductPriceDetails {
	
	@Autowired
	SicarService sicarService;
	
	@Autowired
	ClientService clientService;

	static Logger logger = Logger.getLogger(ProductPriceDetails.class);
	static JTextField id = new JTextField("");
	static JTextField itemIdentifier = new JTextField("");
	static JTextField description = new JTextField("");
	static JTextField exchangeRate = new JTextField("");
	static JTextField purchasePriceAvrg = new JTextField("");
	static JTextField profitPercentage1 = new JTextField("");
	static JTextField profitPercentage2 = new JTextField("");
	static JTextField productPrice1 = new JTextField("");
	static JTextField productPrice2 = new JTextField("");
	static JTextField profitPercentage3 = new JTextField("");
	static JTextField productPrice3 = new JTextField("");
	static JTextField exchangeRateOld = new JTextField("");
	static JFrame  frame;
	private JTable itemTable;
	
	String[] columnNames = {"id","itemIdentifier","exchangeRate","currency","purchasePriceAvrg", "transactionDate"};	
	DefaultTableModel model = new DefaultTableModel(columnNames, 6);
	JTable historicPricesTable  = new JTable(model);
		
	public JFrame getProductPricePanel() {
		

			 frame = new JFrame("Detalle de precios por articulo");
			 frame.setSize(1000, 300);
			 frame.getContentPane().setBackground(Color.WHITE);
			 frame.setLocationRelativeTo(null);

	  try 
         {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
         } catch (Exception e) {
            e.printStackTrace();
         }
		    // HEADER PANEL
		    JPanel headerPanel = new JPanel();
		    Font fnt = new Font("SansSerif", Font.BOLD, 20);
		    headerPanel.setBackground(Color.WHITE);
		    
		    frame.getRootPane().setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK));
		    frame.setUndecorated(true);
		    
		    itemIdentifier.setBorder(new LineBorder(Color.white,1)); 
		    itemIdentifier.setPreferredSize(new Dimension(100, 25));
		    itemIdentifier.setEditable(false);
		    itemIdentifier.setFont(fnt);
		    itemIdentifier.setBackground(Color.WHITE);
		    
		    description.setBorder(new LineBorder(Color.white,1)); 
		    description.setPreferredSize(new Dimension(700, 25));
		    description.setEditable(false);
		    description.setFont(fnt);
		    description.setBackground(Color.WHITE);
		    
		    headerPanel.add(itemIdentifier);
			
		    // LEFT PANEL
			JPanel leftPanel = new JPanel();
			leftPanel.setBackground(Color.WHITE);
			leftPanel.setLayout(new GridBagLayout());
			GridBagConstraints constraints = new GridBagConstraints();
			constraints.insets = new Insets(5, 5, 5, 5);
			id.setVisible(false);
			constraints.gridx = 0; 
			constraints.gridy = 0;
			leftPanel.add(new JLabel("Tipo de Cambio: "), constraints);
			
			exchangeRate.setPreferredSize(new Dimension(200, 29));
			exchangeRate.addActionListener(new ActionListener() {
			      public void actionPerformed(ActionEvent e) {
			    	 //purchasePriceAvrg.setText(String.valueOf((Double.valueOf(exchangeRate.getText())*Double.valueOf(purchasePriceAvrg.getText()))/Double.valueOf(exchangeRate.getText())));
			      }
			    });
			constraints.gridx = 1;
			constraints.gridy = 0; 
			leftPanel.add(exchangeRate, constraints);
			
			constraints.gridx = 0; 
			constraints.gridy = 1; 
			leftPanel.add(new JLabel("Costo Promedio: "), constraints);
			
			purchasePriceAvrg.setPreferredSize(new Dimension(200, 29));
			constraints.gridx = 1;
			constraints.gridy = 1; 
			purchasePriceAvrg.setEditable(false);
			leftPanel.add(purchasePriceAvrg, constraints);

			constraints.gridx = 0; 
			constraints.gridy = 2; 
			//leftPanel.add(new JLabel("% Costo original: "), constraints);
			
			exchangeRateOld.setPreferredSize(new Dimension(200, 29));
			constraints.gridx = 1;
			constraints.gridy = 2; 
			exchangeRateOld.setEditable(false);
			exchangeRateOld.setVisible(false);
			leftPanel.add(exchangeRateOld, constraints);
			
			/*
			constraints.gridx = 0; 
			constraints.gridy = 2; 
			leftPanel.add(new JLabel("% Utilidad 1: "), constraints);
			
			profitPercentage1.setPreferredSize(new Dimension(200, 29));
			constraints.gridx = 1;
			constraints.gridy = 2; 
			profitPercentage1.setEditable(false);
			leftPanel.add(profitPercentage1, constraints);
			
			constraints.gridx = 0; 
			constraints.gridy = 3; 
			leftPanel.add(new JLabel("Precio Unitario 1: "), constraints);

			productPrice1.setPreferredSize(new Dimension(200, 29));
			constraints.gridx = 1;
			constraints.gridy = 3; 
			productPrice1.setEditable(false);
			leftPanel.add(productPrice1, constraints);
			
			constraints.gridx = 0; 
			constraints.gridy = 4; 
			leftPanel.add(new JLabel("% Utilidad 2: "), constraints);
			
			profitPercentage2.setPreferredSize(new Dimension(200, 29));
			constraints.gridx = 1;
			constraints.gridy = 4; 
			profitPercentage2.setEditable(false);
			leftPanel.add(profitPercentage2, constraints);
			
			constraints.gridx = 0; 
			constraints.gridy = 5; 
			leftPanel.add(new JLabel("Precio Unitario 2: "), constraints);

			productPrice2.setPreferredSize(new Dimension(200, 29));
			constraints.gridx = 1;
			constraints.gridy = 5; 
			productPrice2.setEditable(false);
			leftPanel.add(productPrice2, constraints);
			
			constraints.gridx = 0; 
			constraints.gridy = 6; 
			leftPanel.add(new JLabel("% Utilidad 3: "), constraints);
			
			profitPercentage3.setPreferredSize(new Dimension(200, 29));
			constraints.gridx = 1;
			constraints.gridy = 6; 
			profitPercentage3.setEditable(false);
			leftPanel.add(profitPercentage3, constraints);
			
			constraints.gridx = 0; 
			constraints.gridy = 7; 
			leftPanel.add(new JLabel("Precio Unitario 3: "), constraints);

			productPrice3.setPreferredSize(new Dimension(200, 29));
			constraints.gridx = 1;
			constraints.gridy = 7; 
			productPrice3.setEditable(false);
			leftPanel.add(productPrice3, constraints);
			
			*/
			
			JButton saveButton = new JButton("Guardar registro");
			saveButton.addActionListener(new ActionListener() {
		        public void actionPerformed(ActionEvent e) {
		        	logger.info("ACCI�N: Guardar registro.");
		        	updateRecord(true);
		        }
		        
		    });
			JButton recalculateButton = new JButton("Calcular Precios");
			recalculateButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					logger.info("ACCI�N: Calcular Precios.");
					recalculatesPrices(Double.valueOf(exchangeRate.getText()));
				}		        
		    });
			
			JButton exitButton = new JButton("Cerrar");
			exitButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					logger.info("ACCI�N: Cerrar.");
					frame.dispose();
				}		        
		    });
			
			JPanel buttonsPanel = new JPanel();
			buttonsPanel.setBackground(Color.WHITE);
			buttonsPanel.add(saveButton);
			constraints.gridx = 0;
			constraints.gridy = 8; 
			leftPanel.add(buttonsPanel, constraints);

			buttonsPanel.add(exitButton);
			//buttonsPanel.add(recalculateButton);
			constraints.gridx = 1;
			constraints.gridy = 8; 
			leftPanel.add(buttonsPanel, constraints);
			
			// RIGHT PANEL
			historicPricesTable.setFocusable(false);
			historicPricesTable.setRowSelectionAllowed(true);
			historicPricesTable.setPreferredSize(new Dimension(600, 200));
			historicPricesTable.setBounds(5, 5, 5, 5); 
			JScrollPane sp = new JScrollPane(historicPricesTable,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			sp.setBackground(Color.WHITE);

			sp.setPreferredSize(new Dimension(600, 300));
			frame.getContentPane().add(BorderLayout.NORTH, headerPanel);
			frame.getContentPane().add(BorderLayout.WEST, leftPanel);
			frame.getContentPane().add(BorderLayout.EAST, sp);
	
		return frame;
		
	}
	
	public boolean updateRecord(boolean enable) {
		try {
			
			double newRate = Double.valueOf(exchangeRate.getText());
			double currentRate = Double.valueOf(exchangeRateOld.getText());
			if( newRate == currentRate) {
				JOptionPane.showMessageDialog(null, "Debe registrar un tipo de cambio distinto al actual.");
				return false;
			}
			
			if(exchangeRate.getText().isEmpty()) {
				JOptionPane.showMessageDialog(null, "Por favor ingresar un tipo de cambio v�lido.");
				return false;
			}
			
			if(purchasePriceAvrg.getText().isEmpty()) {
				JOptionPane.showMessageDialog(null, "No fue posible obtener el costo promedio.");
				return false;
			}
			
			int input = JOptionPane.showConfirmDialog(null,
					"�Desea actualizar el tipo de cambio? NOTA IMPORTANTE!: Todos los precios de los art�culos en d�lares ser�n actualzados al nuevo tipo de cambio.");
			if (input == 0) {
					logger.info("Inicia actualizaci�n del  tipo de cambio.***");
				    double tCambio = Double.valueOf(exchangeRate.getText());
				    boolean exchangeRateUpdated = sicarService.updateTipoCambioIntoMoneda(tCambio);

				    if(!exchangeRateUpdated) {
						JOptionPane.showMessageDialog(null, "No fue posible actualizar el tipo de cambio centralizado.");
						return false;				    	
				    }
				    
				    logger.info("Obtiene la lista de productos de SicarProductPrices.");
				    List<SicarProductPrice> listp = clientService.getSicarProductDtoList();				    				   				    
				    if(listp != null) {
				    	List<SicarProductPrice> sicarProductList = new ArrayList<SicarProductPrice>();				    	
				    	for(SicarProductPrice o : listp){
				    		
						    HistoricPriceCatalog sicarProductOld = new HistoricPriceCatalog();
						    sicarProductOld.setId(0);
						    sicarProductOld.setItemIdentifier(o.getItemIdentifier());
							sicarProductOld.setExchangeRate(o.getExchangeRate());
							sicarProductOld.setTransactionDate(new Date());
							sicarProductOld.setCurrency("USD");
							sicarProductOld.setPurchasePriceAvrg(o.getPurchasePriceAvrg());
							
							sicarProductOld.setProfitPercentage1(o.getProfitPercentage1());
							sicarProductOld.setProfitPercentage2(o.getProfitPercentage2());
							sicarProductOld.setProfitPercentage3(o.getProfitPercentage3());
							sicarProductOld.setProductPrice1(o.getProductPrice1());
							sicarProductOld.setProductPrice2(o.getProductPrice2());
							sicarProductOld.setProductPrice3(o.getProductPrice3());
							List<HistoricPriceCatalog> hpcList = new ArrayList<>();
							hpcList.add(sicarProductOld);
							clientService.saveHistoricPriceListService(hpcList);
							
							SicarProductPrice sicarProduct = new SicarProductPrice();
							sicarProduct.setId(o.getId());
							sicarProduct.setItemIdentifier(o.getItemIdentifier());
							sicarProduct.setDescription(o.getDescription());
							sicarProduct.setExchangeRate(tCambio);
							
							sicarProduct.setUpdatedDate(new Date());
							sicarProduct.setUpdatedBy("ClientApp");
							sicarProduct.setCurrency("USD");
							
							if(purchasePriceAvrg.getText()!=null) {															
								sicarProduct.setPurchasePriceAvrg(o.getPurchasePriceAvrg());								
								sicarProduct.setProfitPercentage1(o.getProfitPercentage1());
								sicarProduct.setProfitPercentage2(o.getProfitPercentage2());
								sicarProduct.setProfitPercentage3(o.getProfitPercentage3());
								
								//calculates prices
								sicarProduct.setProductPrice1(((o.getProfitPercentage1()/100) + 1) * o.getPurchasePriceAvrg());
								sicarProduct.setProductPrice2(((o.getProfitPercentage2()/100) + 1) * o.getPurchasePriceAvrg());
								sicarProduct.setProductPrice3(((o.getProfitPercentage3()/100) + 1) * o.getPurchasePriceAvrg());
								sicarProduct.setProductPrice1(Math.round(sicarProduct.getProductPrice1() * 1000.0) / 1000.0);
								sicarProduct.setProductPrice2(Math.round(sicarProduct.getProductPrice2() * 1000.0) / 1000.0);
								sicarProduct.setProductPrice3(Math.round(sicarProduct.getProductPrice3() * 1000.0) / 1000.0);
								sicarProductList.add(sicarProduct);
								logger.info(sicarProduct.toString());
							}
				    	}
				    	logger.info("Se insertan registros en la tabla [SicarProductPrices].");
						clientService.saveSicarProductDto(sicarProductList);
						
						logger.info("Actualiza la tabla [articulo] de Sicar.");
						sicarService.updateSicarItemPrice(sicarProductList);
						
						logger.info("Actualiza la tabla [nubeact] de Sicar.");
						sicarService.updateSicarNubeAct(sicarProductList);						
						
						//Actualizar panel de b�squeda
						DefaultTableModel model = (DefaultTableModel)itemTable.getModel();
						model.setRowCount(0);
						
						//Actualizar panel de hist�rico
						DefaultTableModel modelH = (DefaultTableModel)historicPricesTable.getModel();
						modelH.setRowCount(0);
						List<HistoricPriceCatalog> listH = clientService.getHistoricPriceListByItemId(itemIdentifier.getText());
						if(listH != null) {
							for(HistoricPriceCatalog o : listH) {
								modelH.addRow(new Object[]{o.getId(),o.getItemIdentifier(),o.getExchangeRate(), o.getCurrency(),o.getPurchasePriceAvrg(),o.getTransactionDate()});
							}
						}
						logger.info("Registro almacenado.");						
						JOptionPane.showMessageDialog(null, "El registro ha sido almacenado.");
				    }
		}			
		}catch(Exception e) {
			e.printStackTrace();
			logger.error("Error al registrar el nuevo tipo de cambio. ", e);
		}
		
		return true;
	}
	

	public void setHeader(SicarProductPrice obj) {
		id.setText(String.valueOf(obj.getId()));
		itemIdentifier.setText(obj.getItemIdentifier());
		description.setText(obj.getDescription());
		exchangeRate.setText(String.valueOf(obj.getExchangeRate()));
		exchangeRateOld.setText(String.valueOf(obj.getExchangeRate()));
		purchasePriceAvrg.setText(String.valueOf(Math.round(obj.getPurchasePriceAvrg()*1000)/1000.0));
		profitPercentage1.setText(String.valueOf(Math.round(obj.getProfitPercentage1()*1000)/1000.0));
		productPrice1.setText(String.valueOf(Math.round(obj.getProductPrice1()*1000)/1000.0));
		profitPercentage2.setText(String.valueOf(Math.round(obj.getProfitPercentage2()*1000)/1000.0));
		productPrice2.setText(String.valueOf(Math.round(obj.getProductPrice2()*1000)/1000.0));
		profitPercentage3.setText(String.valueOf(Math.round(obj.getProfitPercentage3()*1000)/1000.0));
		productPrice3.setText(String.valueOf(Math.round(obj.getProductPrice3()*1000)/1000.0));
		
		DefaultTableModel model = (DefaultTableModel)historicPricesTable.getModel();
		model.setRowCount(0);
		List<HistoricPriceCatalog> list = clientService.getHistoricPriceListByItemId(obj.getItemIdentifier());
		if(list != null) {
			for(HistoricPriceCatalog o : list) {
				model.addRow(new Object[]{o.getId(),o.getItemIdentifier(),o.getExchangeRate(), o.getCurrency(),o.getPurchasePriceAvrg(),o.getTransactionDate()});
			}
		}
		
       try {
				
		historicPricesTable.getColumnModel().getColumn(0).setMinWidth(0);
		historicPricesTable.getColumnModel().getColumn(0).setMaxWidth(0);
		historicPricesTable.getColumnModel().getColumn(0).setWidth(0);
		
		historicPricesTable.getColumnModel().getColumn(1).setMinWidth(0);
		historicPricesTable.getColumnModel().getColumn(1).setMaxWidth(0);
		historicPricesTable.getColumnModel().getColumn(1).setWidth(0);
		historicPricesTable.getColumnModel().getColumn(2).setHeaderValue("T.C.");
		historicPricesTable.getColumnModel().getColumn(3).setHeaderValue("Moneda");
		historicPricesTable.getColumnModel().getColumn(4).setHeaderValue("Costo Promedio");
		historicPricesTable.getColumnModel().getColumn(5).setHeaderValue("Fecha ");
		historicPricesTable.getColumnModel().getColumn(6).setHeaderValue("Fecha Modificaci�n");
		
       }catch(Exception e) {
    	   
       }

	}
	
	public void recalculatesPrices(double tcValue) {
		
		
		double percentage1=Double.valueOf(profitPercentage1.getText())/100;
		double percentage2=Double.valueOf(profitPercentage2.getText())/100;
		double percentage3=Double.valueOf(profitPercentage3.getText())/100;
		//calculates prices
		productPrice1.setText(String.valueOf(Math.round(((percentage1+1) *Double.valueOf(purchasePriceAvrg.getText()))* 1000.0) / 1000.0));
		productPrice2.setText(String.valueOf(Math.round(((percentage2+1) *Double.valueOf(purchasePriceAvrg.getText()))* 1000.0) / 1000.0));
		productPrice3.setText(String.valueOf(Math.round(((percentage3+1) *Double.valueOf(purchasePriceAvrg.getText()))* 1000.0) / 1000.0));
		
	}
	
	public void setParentTable(JTable itemTable) {
		this.itemTable = itemTable;
	}
	
}
