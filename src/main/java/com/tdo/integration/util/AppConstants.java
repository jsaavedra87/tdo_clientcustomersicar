package com.tdo.integration.util;

public final class AppConstants {

	
	public static final String NCSTATUSES = "NCSTATUSES";
	public static final String SIN_FACTURAR ="Sin Facturar";
	public static final String ERROR_300_DESC ="Ocurri� un error inesperado.";
	public static final String ERROR_300="300";
	public static final String NC_NEW_STATUS="NEW";
	public static final String NC_ERROR_STATUS="ERROR";
}
